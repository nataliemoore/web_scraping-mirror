# -*- coding: utf-8 -*-
"""
Created on Wed Jul 09 08:21:41 2014

@author: nataliecmoore

Script Name: CME_FEEDER_CATTLE_INDEX_SCRAPER

Purpose:
Retrieve the past seven day feeder cattle index.

Approach:
Stored the contents of the CME webpage where the feeder cattle index was located 
and then used string parsing to find the 7 day index ending on the previous
day. The data was then formatted for upload to quandl.com.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""

import urllib2
import datetime
import pandas as pd
import sys

url = 'http://www.cmegroup.com/wrappedpages/misc/FCLRP.html' # website where the data is located
req = urllib2.Request(url, headers={ 'User-Agent': 'Mozilla/30.0' }) # request url as a web browser 
site_contents = urllib2.urlopen(req).read() # read url and store contents in site_contents

beginning = site_contents.find('7 days ending') # data begins after the phrase '7 days ending'
end = site_contents.find('<br>', beginning) # data ends at the line break
date_begin = site_contents.find('  ', beginning) # stores the index of the space before the date begins
date_end = site_contents.find('is', beginning) # stores the index of where the date ends
# retrieves the date from the website (in the form mm/dd/YYYY) and converts into a datetime object
date = datetime.datetime.strptime(site_contents[date_begin:date_end].strip(), '%m/%d/%Y') 
date = date + datetime.timedelta(days = 1) # adds one day to the date to report previous day's data
# finds the value of the feeder cattle index and removes extra spaces around value
value = site_contents[date_end + 2:end].strip() 
headings = ['Date', 'Feeder Cattle Index ($/cwt)']
data = {'Date': [date.strftime('%Y%m%d')], 'Feeder Cattle Index ($/cwt)': [value]}
data_df = pd.DataFrame(data, columns = headings)
data_df.index = data_df['Date']
data_df = data_df.drop('Date', 1)
quandl_code = 'CME_FEEDER_CATTLE_INDEX\r'# build unique quandl code
reference_text = '  Historical figures can be verified' \
'\n  at http://markets.ft.com/research//Tearsheets/PriceHistoryPopup?symbol=ICX:CME\n' 
print 'code: ' + quandl_code + '\n'
print 'name: CME Feeder Cattle Index \n'
print 'description: Daily feeder cattle index calculated over the past 7 days. '\
      '\n  All prices are in $/cwt. \n'\
      + reference_text + '\n'
print 'reference_url: http://www.cmegroup.com/wrappedpages/misc/FCLRP.html\n'
print 'frequency: daily\n'
print 'private: false\n'
print '---\n'
data_df.to_csv(sys.stdout)
print '\n'
print '\n'