# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 10:03:16 2014

@author: nataliecmoore

Script Name: CME_FC_SCRAPER

Purpose:

Approach:

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""

import urllib2

url = 'http://www.cmegroup.com/trading/agricultural/livestock/feeder-cattle.html' # stores url of report
req=urllib2.Request(url, headers={ 'User-Agent': 'Mozilla/30.0' }) # request url as a web browser 
site_contents=urllib2.urlopen(req).read() # read url and store contents in site_contents

start_index=site_contents.find('quotesFuturesProductTable1_GFQ4_priorSettle')
start_index=site_contents.find('>', start_index)
end_index=site_contents.find('</td>', start_index)
settle_price=site_contents[start_index+1:end_index]