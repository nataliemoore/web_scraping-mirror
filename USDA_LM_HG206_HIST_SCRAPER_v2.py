# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 10:53:52 2014

@author: nataliecmoore

Script Name: USDA_LM_HG206_HIST_SCRAPER_v2

Purpose:
Retrieve daily Iowa/Minnesota hog data from the LM_HG206 report for the past 
5 days via the USDA AMS web service for upload to Quandl.com. The script pulls
data for head count, price range, and weighted average price.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the head count, price range, and weighted average price for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/06/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 150 #set by user of program
startdate = datetime.datetime.now()  # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the data is listed and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day each iteration
    month = str(date.month) # store the month in string format   
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_HG206' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x = x + 1 # increment x to skip that day and go to the next
        continue
    starting_index = site_contents.find(':', site_contents.find('Barrows & Gilts')) # stores index of colon before head count
    end_head = site_contents.find('\r\n', starting_index) # stores index of the end of the head count
    head = float(site_contents[starting_index+1:end_head].strip().replace(',','').replace('*','')) # store the head count, replace commas, and convert to a float
    if site_contents.rfind('ue to confidentiality', starting_index, site_contents.find('IOWA/MINNESOTA DAILY DIRECT NEGOTIATED'))!=-1:
        low_price = 0 # store a zero for the low price
        high_price = 0 # store a zero for the high price
        weighted_average = 0 # store a zero for the weighted average
    else:
        price_start = site_contents.find('$', site_contents.find('Base Price Range')) # store index of beginning of price range
        hyphen = site_contents.find('-', price_start) # store index of hyphen separating min and max price
        low_price = float(site_contents[price_start+1:hyphen].strip().replace('*','')) # store min price
        price_end = site_contents.find(',', hyphen) # store index of comma that marks end of price range
        high_price = float(site_contents[hyphen+1:price_end].strip().replace('$','').replace('*',''))
        weighted_average_begin = site_contents.find('$', price_end) # find index of $ before weighted average
        weighted_average_end = site_contents.find('\r\n', weighted_average_begin) # find index of line break after weighted average
        weighted_average = float(site_contents[weighted_average_begin+1:weighted_average_end].strip().replace('*','')) # store weighted average
    if site_contents.find('ue to confidentiality', site_contents.find('Barrows & Gilts (live basis,'),site_contents.find('BARROWS & GILTS PURCHASE BY STATE OF ORIGIN'))!=-1:
        low_price2 = 0
        high_price2 = 0
        weighted_average2 = 0
    else:
        price_start2 = site_contents.find('$', site_contents.find('Barrows & Gilts (live basis')) 
        hyphen2 = site_contents.find('-', price_start2) 
        low_price2 = float(site_contents[price_start2+1:hyphen2].strip().replace('*',''))  
        price_end2 = site_contents.find(',', hyphen2)
        high_price2 = float(site_contents[hyphen2+1:price_end2].strip().replace('$','').replace('*',''))
        weighted_average_begin2 = site_contents.find('$', price_end2) # find index of $ before weighted average
        weighted_average_end2 = site_contents.find('\r\n', weighted_average_begin2) # find index of line break after weighted average
        weighted_average2 = float(site_contents[weighted_average_begin2+1:weighted_average_end2].strip().replace('*','')) # store weighted average
    
    headings = ['Date', 'Lean Hog Head Count', 'Lean Hog Low Price', 'Lean Hog High Price', 'Lean Hog Weighted Average Price', \
          'Live Hog Low Price', 'Live Hog High Price', 'Live Hog Weighted Average Price']
    data = {'Date': [date.strftime('%Y-%m-%d')], 'Lean Hog Head Count': [head], 'Lean Hog Low Price': [low_price], \
      'Lean Hog High Price': [high_price], 'Lean Hog Weighted Average Price': [weighted_average], 'Live Hog Low Price': [low_price2],\
      'Live Hog High Price': [high_price2], 'Live Hog Weighted Average Price': [weighted_average2]}
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_LM_HG206'  # build unique quandl code
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' \
    '  All pricing is on a per CWT (100 lbs) basis.'
    print 'code: ' + quandl_code
    print 'name: Iowa/Minnesota Daily Direct Hog Report' 
    print 'description: Daily head count, price range and weighted average price' \
    '\n  from the USDA LM_HG206 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS).'+ reference_text
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg206.txt'
    print 'frequency: daily'
    print 'private: false'
    print '---'
    data_df.to_csv(sys.stdout)
    print ''
    print ''
    x = x + 1