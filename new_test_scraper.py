# -*- coding: utf-8 -*-
"""
Created on Fri May 30 08:00:53 2014

@author: nataliecmoore
"""

import datetime 
import urllib2
import pandas as pd
import sys
import re
import pytz
from pyparsing import *

# store report in variable
url="http://www.ams.usda.gov/mnreports/lm_pk602.txt"
site_contents=urllib2.urlopen(url).read()

# initialize lists that will later hold values for each cut
cuts=['Loin', 'Butt', 'Picnic', 'Sparerib', 'Ham', 'Belly', 'Jowl','Trim', 'Variety', 'AI']  
cuts_sublist=[]    #holds names of each cut
weight=[]          #holds weight of each cut (in lbs)
min_cost=[]        #holds minimum cost of each cut (in cents)
max_cost=[]        #holds maximum cost of each cut (in cents)
wtd_avg=[]         #holds weighted average of each cut's cost
primal=[]

line_list=[]
lines= re.compile("\r?\n").split(site_contents)
pattern=re.compile("\s\s+")
for line in lines:
    newline=pattern.split(line)
    line_list.append(newline)
line_list=[line_list[x] for x in range(len(line_list)) if len(line_list[x])!=0]
line_list=[line_list[x] for x in range(len(line_list)) if line_list[x][0]!=""]
line_list=[line_list[x] for x in range(len(line_list)) if (line_list[x][0][0]=="-" or line_list[x][0][0]==" ")]

x=0
while x!=1:
   if line_list[x][0][0]!='-':
       x=1
   else:
       del line_list[x]
x=0
primal_index=0
while x<len(line_list):
    if line_list[x][0][0]=='-':
        del line_list[x]
        primal_index=primal_index+1
    elif len(line_list[x])==1:
        del line_list[x]
    else:
        primal.append(cuts[primal_index])
        cuts_sublist.append(line_list[x][0])
        if line_list[x][1].find(' ')==-1:
            weight.append(line_list[x][1])
        if line_list[x][1].find(' ')!=-1:
            s_index=line_list[x][1].find(' ')
            weight.append(line_list[x][1][0:s_index])
            line_list[x].insert(2, line_list[x][1][s_index:])
        if len(line_list[x])!=2:
            dash_index=line_list[x][2].find("-")
            if dash_index==-1:
                min_cost.append(line_list[x][2])
                max_cost.append(line_list[x][3][3:])
                wtd_avg.append(line_list[x][4])
            else:            
                min_cost.append(line_list[x][2][:dash_index])
                max_cost.append(line_list[x][2][dash_index+1:])
                wtd_avg.append(line_list[x][3])
        else:
            min_cost.append("-")
            max_cost.append("-")
            wtd_avg.append("-")
        x=x+1
        
n=0
while n<len(weight):
    weight[n]=weight[n].replace(',', '')
    weight= [x if x!='-' else '0' for x in weight] #change each empty entry to 0
    n=n+1
weight=[int(i) for i in weight]

# Changes each entry in lists "min_cost" "max_cost" and "wtd_avg" from a string
# to a floating point 
s_list=[min_cost, max_cost, wtd_avg]
s=0
while s <3:
    n=0
    while n<len(s_list[s]):
        s_list[s]= [x if x!='-' else '0' for x in s_list[s]] #change each empty entry to 0
        n=n+1
    s_list[s]=[float(i) for i in s_list[s]]
    s=s+1
min_cost=s_list[0]
max_cost=s_list[1]
wtd_avg=s_list[2]


s=0
while s<len(cuts_sublist):
    date=datetime.datetime.now(pytz.timezone('US/Eastern')).strftime("%Y-%m-%d")
    cuts_headings = [ 'Date', 'Primal', 'LBS', '$ Low', '$ High', '$ WgtAvg']
    data={'Date': [date], 'Primal': [primal[s]], 'LBS': [weight[s]], '$ Low': [min_cost[s]], '$ High': [max_cost[s]], '$ WgtAvg': [wtd_avg[s]]}
    cuts_df = pd.DataFrame(data, columns = cuts_headings)
    cuts_df.index = cuts_df['Date']
    cuts_df = cuts_df.drop('Date', 1).drop('Primal', 1)
    replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
    remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
    cut1 = replace.sub('_', cuts_sublist[s]) # replace certain characters with '_'
    cut2 = remove.sub('', cut1).upper() # remove certain characters and convert to upper case
    cut2 = cut2.translate(None, '-') # ensure '-' character is removed
    quandl_code = 'USDA_LM_PK602_' + cut2 # build unique quandl code
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.'
    print 'code: ' + quandl_code
    print 'name: Pork ' +primal[s]+' - '+cuts_sublist[s]
    print 'description: Daily total pounds, low price, high price and weighted average price ' \
    '\n  from the USDA LM_PK602 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS). This dataset covers ' + cuts_sublist[s] + '.\n'\
    + reference_text
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_pk602.txt'
    print 'frequency: daily'
    print 'private: false'
    print '---'
    cuts_df.to_csv(sys.stdout)
    print ''
    print ''
    s=s+1


    
    