# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 12:46:03 2014

@author: nataliecmoore

Script Name: USDA_LM_HG206_HIST_SCRAPER

Purpose:
Retrieve daily Iowa/Minnesota hog data from the LM_HG206 report for the past
four days via the USDA LMR web service for upload to Quandl.com. The script
pulls data for head count, price range, and weighted average price.

Approach:
Accessed the data from the LMR web service in XML format. Looped through each XML sub-section
until the relevant data was found. Then looped through each date and created a table for 
each that was formatted for upload to quandl.com.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/17/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
from lxml import objectify

# Set the date range for the report (4 days to ensure all data is captured)
startdate = datetime.datetime.now() - 4 * pto.BDay()
startdate = startdate.strftime('%m/%d/%Y')

target_url = 'http://mpr.datamart.ams.usda.gov/ws/report/v1/hogs/LM_HG206?filter={%22filters%22:[{%22fieldName%22:%22Report%20Date%22,%22operatorType%22:%22GREATER%22,%22values%22:[%22'+startdate+'%22]}]}'

fileobj = urllib2.urlopen(target_url).read()

data=[]

root = objectify.fromstring(fileobj)

# Loops through each report date and adds the date, low price, high price, and weighted average
# price to "data"
for report_date in root.report.iterchildren():
    date=report_date.attrib['report_date']
    for report in report_date.iterchildren():
        if report.attrib['label']=='Barrows/Gilts (producer/packer sold)':
            for item in report.iterchildren():
                if item.attrib['purchase_type']=='Negotiated (carcass basis)':
                   data.append([date, item.attrib['head_count'], item.attrib['price_low'], item.attrib['price_high'], item.attrib['wtd_avg']])
# Loops through each date in data and creates a table of the data. The table is then formatted
# so it can be uploaded to quandl.
x=0
while x<len(data):  
    headings=['Date', 'Head Count', 'Low Price', 'High Price', 'Weighted Average Price']
    hg_data={'Date': [data[x][0]], 'Head Count': [data[x][1]], 'Low Price': [data[x][2]], 'High Price': [data[x][3]], \
      'Weighted Average Price': [data[x][4]]}
    data_df = pd.DataFrame(hg_data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_LM_HG206'  # build unique quandl code
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' \
    '  All pricing is on a per CWT (100 lbs) basis.'
    print 'code: ' + quandl_code
    print 'name: Iowa/Minnesota Daily Direct Hog Report' 
    print 'description: Daily head count, price range and weighted average price' \
    '\n  from the USDA LM_HG206 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS).'+ reference_text
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg206.txt'
    print 'frequency: daily'
    print 'private: false'
    print '---'
    data_df.to_csv(sys.stdout)
    print ''
    print ''
    x=x+1


