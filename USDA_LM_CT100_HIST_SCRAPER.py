# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 08:23:19 2014

@author: nataliecmoore

Script Name: USDA_LM_CT100_HIST_SCRAPER

Purpose:
Retrieve daily beef slaughter data from the LM_CT100 report via the USDA LMR
web service for upload to Quandl.com. The script pulls data for head count,
weight range, price range, average weight, and average price for steers and 
heifers of grade categories set by the USDA for the past four days.

Approach:
Accessed the data from the LMR web service in XML format. Looped through each XML sub-section
until the relevant data was found. Then looped through each date and created a table for 
each that was formatted for upload to quandl.com.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/17/2014      Natalie Moore   Initial development/release

"""


import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re
from lxml import objectify

# Set the date range for the report (4 days to ensure all data is captured)
startdate = datetime.datetime.now() - 4 * pto.BDay()
startdate = startdate.strftime('%m/%d/%Y')

target_url = 'http://mpr.datamart.ams.usda.gov/ws/report/v1/cattle/LM_CT100?filter={%22filters%22:[{%22fieldName%22:%22Report%20Date%22,%22operatorType%22:%22GREATER%22,%22values%22:[%22'+startdate+'%22]}]}'

fileobj = urllib2.urlopen(target_url).read()

data=[] # initialize list which will later hold data

root = objectify.fromstring(fileobj)

for report_date in root.report.iterchildren(): # processing must be repeated for each day covered by the report
    date = report_date.attrib['report_date'] # the prior day is listed 
    date=datetime.datetime.strptime(date, '%m/%d/%Y')
    date=(date-1*pto.BDay()).strftime('%m/%d/%Y') # report is reporting on previous business day
    for report in report_date.iterchildren():
        if report.attrib['label']=='Detail':
            for item in report.iterchildren():
                new_data=[date, item.attrib['class_description'], item.attrib['selling_basis_description'],\
                             item.attrib['grade_description'], item.attrib['head_count'], item.attrib['weight_range_low'], \
                             item.attrib['weight_range_high'], item.attrib['price_range_low'],\
                             item.attrib['price_range_high'], item.attrib['weight_range_avg'], \
                             item.attrib['weighted_avg_price']]
                new_data=[y if y!='null' else 0 for y in new_data] # convert null elements to 0
                data.append(new_data) 
x=0
while x<len(data):
    headings = [ 'Date', 'Head Count', 'Weight Low', 'Weight High','$ Low', '$ High', 'Avg Weight', 'Avg Price']
    ct_data={'Date': [data[x][0]], 'Head Count': [data[x][4]], 'Weight Low': [data[x][5]],\
    'Weight High': [data[x][6]], '$ Low': [data[x][7]],'$ High': [data[x][8]], \
    'Avg Weight': [data[x][9]], 'Avg Price': [data[x][10]]}
    data_df = pd.DataFrame(ct_data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    replace = re.compile('[ /]') # list of characters to be replaced
    remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed 
    name1 = replace.sub('_', data[x][3]) # replace certain characters with '_'
    name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
    name2 = name2.translate(None, '-') # ensure '-' character is removed
    if data[x][2]=='Dressed': # set basis and basis_name which are used in creating the code, name, and description
        basis='DDB'
        basis_name='Dressed Delivered'
    if data[x][2]=='Live':
        basis='LFB'
        basis_name='Live FOB'
    quandl_code = 'USDA_LM_CT100_' +basis+'_'+(data[x][1]+'s').upper()+'_'+name2 # build unique quandl code
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' \
    '  All pricing is on a per CWT (100 lbs) basis.'
    print 'code: ' + quandl_code
    print 'name: Five Area Daily Beef Slaughter- ' +basis_name+" "+(data[x][1]+'s').title()+" "+data[x][3]
    print 'description: Daily head count, weight range, price range, average weight, and average price' \
    '\n  from the USDA LM_CT100 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS). This dataset covers '+basis_name+' ' +(data[x][1]+'s').title() +" "+data[x][3] + '.\n'\
    + reference_text
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_ct100.txt'
    print 'frequency: daily'
    print 'private: false'
    print '---'
    data_df.to_csv(sys.stdout)
    print ''
    print ''
    x=x+1
                             
                             