# -*- coding: utf-8 -*-
"""
Created on Tue Jun 17 14:00:27 2014

@author: nataliecmoore

Script Name: USDA_LM_HG210_HIST_SCRAPER

Purpose:
Retrieve daily Eastern Cornbelt USDA data from the LM_HG210 report for the past 4 days 
via the USDA LMR web service for upload to Quandl.com. The script pulls data for 
the base price range and the weighted average base price for slaughtered hogs.

Approach:
Accessed the data from the LMR web service in XML format. Looped through each XML sub-section
until the relevant data was found. Then looped through each date and created a table for 
each that was formatted for upload to quandl.com.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/17/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
from lxml import objectify

# Set the date range for the report (4 days to ensure all data is captured)
startdate = datetime.datetime.now() - 4 * pto.BDay()
startdate = startdate.strftime('%m/%d/%Y')

target_url = 'http://mpr.datamart.ams.usda.gov/ws/report/v1/hogs/LM_HG210?filter={%22filters%22:[{%22fieldName%22:%22Report%20Date%22,%22operatorType%22:%22GREATER%22,%22values%22:[%22'+startdate+'%22]}]}'

fileobj = urllib2.urlopen(target_url).read()

data=[]

root = objectify.fromstring(fileobj)

for report_date in root.report.iterchildren():
    date=report_date.attrib['report_date']
    for report in report_date.iterchildren():
        if report.attrib['label']=='Barrows/Gilts (producer/packer sold)':
            for item in report.iterchildren():
                if item.attrib['purchase_type']=='Negotiated (carcass basis)':
                   new_data=[date, item.attrib['price_low'], item.attrib['price_high'], item.attrib['wtd_avg']]
                   new_data=[y if y!='null' else 0 for y in new_data]
                   data.append(new_data)
                   
x=0
while x<len(data):
    headings=[ 'Date', 'Minimum Base Price', 'Maximum Base Price', 'Weighted Average']
    hg_data={'Date': [data[x][0]], 'Minimum Base Price': [data[x][1]], 'Maximum Base Price': [data[x][2]], \
    'Weighted Average': [data[x][3]]}
    data_df=pd.DataFrame(hg_data, columns=headings)
    data_df.index=data_df['Date']
    data_df=data_df.drop('Date', 1)
    quandl_code='USDA_LM_HG210_EASTERN_BASE_PRICE\r'
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' 
    print 'code: ' + quandl_code+'\n'
    print 'name: Eastern Cornbelt Hog Report- Negotiated Purchase Base Price \n'
    print 'description: Eastern Cornbelt daily negotiated purchase base price including minimum/maximum base price and weighted average ' \
    'from the USDA LM_HG210 report published by the USDA Agricultural Marketing Service ' \
    '(AMS).  \n'\
    + reference_text+'\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg210.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x=x+1