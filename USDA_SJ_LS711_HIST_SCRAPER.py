# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 11:38:03 2014

@author: nataliecmoore

Script Name: USDA_SJ_LS711_HIST_SCRAPER

Purpose:

Approach:

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/20/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Suppress, ZeroOrMore, Word, alphas, printables, nums, Literal
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 150 #set by user of program
startdate = datetime.datetime.now()
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay()
    if date.weekday() != 3:
        x = x + 1
    else:
        report_date = date - datetime.timedelta(days = 12)
        month = str(date.month)    
        if len(month) == 1:
            month = '0' + month
        string_date = date.strftime('%Y%m%d')
        if string_date == '20110428':
           string_date = '20110427'
        try:
            target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/SJ_LS711' + string_date + '.TXT'         
            site_contents = urllib2.urlopen(target_url).read()
        except urllib2.HTTPError:
            x = x + 1
            continue
        day = []
        number_cattle_slaughter = []
        starting_day = site_contents.find('Monday', site_contents.find('Bison')) # store index of beginning of first data section
        ending_day = site_contents.find('---', starting_day) # store index of end of first data section
        section = site_contents[starting_day:ending_day] # store data section
        unparsed = [] # list that will later hold unparsed lines
        parsed = [] # list that will later hold parsed lines
        # Loops through first section of data and finds the slaughter for each day
        while site_contents.find('\r\n', starting_day, ending_day) != -1:
            end_line = site_contents.find('\r\n', starting_day)
            unparsed.append(site_contents[starting_day:end_line])
            starting_day = end_line+2
        cattle_slaughter = (Word(alphas) + Word(printables)) | (Word(alphas) + Literal("-")) # grammar for daily cattle slaughter
        # Loops through each unparsed line and parses it, appending to number_cattle_slaughter
        z = 0
        while z < len(unparsed):
            parsed.append(cattle_slaughter.parseString(unparsed[z]))
            day.append(parsed[z][0])
            number_cattle_slaughter.append(parsed[z][1])
            z = z + 1
        number_cattle_slaughter = [float(g.replace(',','').replace('-','0')) for g in number_cattle_slaughter] # remove commas and convert each value to a float
        starting = site_contents.find('Beef', site_contents.find('Meat Production, Live Weight and Dressed Weight')) # store index of beginning of second data section
        weight = Suppress(Literal('Beef')) + Word(printables) # grammar for finding weight of beef production
        weight = float((weight.parseString(site_contents[starting:]))[0])
        starting = site_contents.find('Cattle', starting)
        word = Suppress(Word(alphas)) + Word(nums)
        weights = Suppress(Word(alphas)) + Word(printables) + Word(nums) + word*4
        average_live_weight = float((weights.parseString(site_contents[starting:]))[0].replace(',',''))
        average_dressed_weight = float((weights.parseString(site_contents[starting:]))[1])
        average_weights = weights.parseString(site_contents[starting:])[2:]
        starting = site_contents.find('Cattle', site_contents.find('Federally Inspected Slaughter Head & Percentage by Class'))
        suppress = Suppress(Word(printables))
        word = Word(alphas) + ZeroOrMore(Word(alphas))
        number = Word(printables)
        cattle_head = suppress + Suppress(ZeroOrMore(Literal(':'))) + (word + Suppress(ZeroOrMore(Literal(':'))) + number) + ( Suppress(ZeroOrMore(Literal(':'))) + word + Suppress(ZeroOrMore(Literal(':'))) + number + Suppress(ZeroOrMore(Literal(':'))) + suppress ) * 5
        y = cattle_head.parseString(site_contents[starting:])
        names = [y[0], y[2], y[4], y[6] + ' ' + y[7], y[9] + ' ' + y[10], y[12]]
        total_head = [y[1],y[3],y[5],y[8], y[11], y[13]]
        total_head = [float(i.replace(',','.')) for i in total_head]
        dates = []
        z = 5
        while z >= 0:
            dates.append((date - datetime.timedelta(hours = 24 * z)).isoformat())
            z = z - 1
        headings = [ 'Date', 'Actual Cattle Slaughter']
        dates = [g[:g.find('T')] for g in dates]
        data = { 'Date': dates, 'Actual Cattle Slaughter': number_cattle_slaughter }
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'USDA_SJ_LS711_ACTUAL_CATTLE_SLAUGHTER\n' # build unique quandl code
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' \
        '  All pricing is on a per CWT (100 lbs) basis.\n'
        print 'code: ' + quandl_code + '\n'
        print 'name: Actual Cattle Slaughter Under Federal Inspection\n '
        print 'description: Actual Cattle Slaughter\n ' \
        '\n  from the USDA SJ_LS711 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/sj_ls711.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        headings = [ 'Date', 'FI Beef Production (million pounds)']
        data = {'Date': [report_date.strftime('%Y%m%d')], 'FI Beef Production (million pounds)': [weight]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'USDA_SJ_LS711_FI_BEEF_PRODUCTION\n' # build unique quandl code
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' \
        '  All pricing is on a per CWT (100 lbs) basis.\n'
        print 'code: ' + quandl_code + '\n'
        print 'name: FI Beef Production\n '
        print 'description: Weekly beef production (million pounds) ' \
        '\n  from the USDA SJ_LS711 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/sj_ls711.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        headings = [ 'Date', 'Average Live Weight', 'Average Dressed Weight']
        data = {'Date': [report_date.strftime('%Y%m%d')], 'Average Live Weight': [average_live_weight], \
        'Average Dressed Weight':[average_dressed_weight]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'USDA_SJ_LS711_FI_BEEF_AVERAGE_WEIGHTS\n' # build unique quandl code
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' \
        '  All pricing is on a per CWT (100 lbs) basis.\n'
        print 'code: ' + quandl_code + '\n'
        print 'name: FI Beef Production- Average Weights\n '
        print 'description: Average live weight and average dressed weight of federally inspected beef ' \
        '\n  from the USDA SJ_LS711 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/sj_ls711.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        name_labels = [ 'Steers' , 'Heifers', 'Cows' , 'Bulls' ]
        z = 0
        while z < len(average_weights):
            headings = [ 'Date', 'Average Weight']
            data = { 'Date': [report_date.strftime('%Y%m%d')], 'Average Weight': [average_weights[z]] }
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            quandl_code = 'USDA_SJ_LS711_AVG_WEIGHT_' + name_labels[z].upper() # build unique quandl code
            reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
            '\n  at http://mpr.datamart.ams.usda.gov.\n' \
            '  All pricing is on a per CWT (100 lbs) basis.\n'
            print 'code: ' + quandl_code + '\n'
            print 'name: FI Beef Production- ' + name_labels[z] + ' Average Weight\n '
            print 'description: Average weight of federally inspected ' + name_labels[z]+ \
            '\n  from the USDA SJ_LS711 report published by the USDA Agricultural Marketing Service ' \
            '\n  (AMS). \n'\
            + reference_text + '\n'
            print 'reference_url: http://www.ams.usda.gov/mnreports/sj_ls711.txt\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
            z = z + 1
        z = 0
        while z < len(total_head):
            headings = [ 'Date', 'Total Head (000)']
            data = { 'Date': [report_date.strftime('%Y%m%d')], 'Total Head (000)': [total_head[z]] }
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            quandl_code = 'USDA_SJ_LS711_HEAD_' + names[z].upper().replace(' ','_') # build unique quandl code
            reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
            '\n  at http://mpr.datamart.ams.usda.gov.\n' \
            '  All pricing is on a per CWT (100 lbs) basis.\n'
            print 'code: ' + quandl_code + '\n'
            print 'name: FI Beef Production- ' + names[z].title() + ' Total Head'
            print 'description: Number of head of federally inspected beef '+ \
            '\n  from the USDA SJ_LS711 report published by the USDA Agricultural Marketing Service ' \
            '\n  (AMS). \n'\
            + reference_text + '\n'
            print 'reference_url: http://www.ams.usda.gov/mnreports/sj_ls711.txt\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
            z = z + 1
        x = x + 1
         
        
