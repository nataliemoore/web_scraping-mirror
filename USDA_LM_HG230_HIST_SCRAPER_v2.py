# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 12:19:04 2014

@author: nataliecmoore

Script Name: USDA_LM_HG230_HIST_SCRAPER_v2

Purpose:
Retrieve the prior day national sow purchase USDA data for the past 5 business 
days from the LM_HG230 report via the USDA AMS web service for upload to 
Quandl.com. The script pulls data for the head count, average weight, 
price range, and weighted average price for purchased sows in the weight ranges: 
300-399, 400-449, 450-499, 500-549, and 550+.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing and 
pyparsing to obtain the head count, average weight, price range, and weighted 
average price for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Literal, Word, nums, ZeroOrMore, printables
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys


num_days = 5 # number of previous days to find data for
startdate = datetime.datetime.now() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the data is listed and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day from the start date each iteration
    month = str(date.month)    # store the month as a string
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_HG230' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x=x+1        
        continue
    start_index = site_contents.find('Sows Purchased (Live and Carcass Basis)')
    labels = [ '300-399', '400-449', '450-499', '500-549', '550/up' ]
    y = 0
    parsed = []
    # Loops through each label in labels and parses its line of data on the website.
    # Then it creates a table with the parsed data elements and moves to the next label.
    while y < len(labels):
        label_index = site_contents.find(labels[y], start_index) # index of labels[x] on the website
        #grammar for each line of data    
        nonempty_line = Literal(labels[y]) + Word(nums+',') + Word(nums) + Word(nums+'.'+'-') + Word(nums+'.')
        empty_line = Literal(labels[y]) + ZeroOrMore(Word(printables))        
        line_grammar = nonempty_line | empty_line        
        line_end = site_contents.find('\r\n', label_index) # index of the end of the line to be parsed
        parsed = line_grammar.parseString(site_contents[label_index:line_end]).asList() # parses line and converts to list
        headings = [ 'Date', 'Head Count', 'Avg Wgt', 'Low Price', 'High Price', 'Wtd Avg Price' ]
        parsed = [p for p in parsed if p!='*']        
        if len(parsed) == 5:       
            parsed.append(parsed[4]) # add the weighted average to end of the list because split on next line will overwrite parsed[4]
            [ parsed[3], parsed[4] ] = parsed[3].split('-') # split the price range into low price and high price
            data = { 'Date': [string_date], 'Head Count': [parsed[1]], 'Avg Wgt': [parsed[2]], 'Low Price': [parsed[3]], \
           'High Price': [parsed[4]], 'Wtd Avg Price': [parsed[5]] }
        else:
            data = { 'Date': [string_date], 'Head Count': [parsed[1].replace('*','0')], 'Avg Wgt':[parsed[2].replace('*','0')], \
            'Low Price': [0], 'High Price': [0], 'Wtd Avg Price': [0] }
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'USDA_LM_HG230_' + parsed[0].replace('-', '_').replace('/', '_') + '\r'# build unique quandl code
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' 
        print 'code: ' + quandl_code + '\n'
        print 'name: National Daily Sows Purchased- ' + parsed[0] + ' pounds\n'
        print 'description: National daily direct sow and boar report. This dataset contains '\
        ' head count, average weight, price range, and weighted average for sows in the weight range '+ parsed[0] +\
        ' from the USDA LM_HG230 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS).\n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg230.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1
    x = x + 1