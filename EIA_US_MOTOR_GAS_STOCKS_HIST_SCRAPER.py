# -*- coding: utf-8 -*-
"""
Created on Tue Jul 08 10:16:09 2014

@author: nataliecmoore

Script Name: EIA_US_MOTOR_GAS_STOCKS_HIST_SCRAPER

Purpose:
Find the weekly U.S. ending stock of gasoline for the past 3 weeks from the 
report published by the U.S. Energy Information Administration.

Approach:
Looped through each report and used string parsing to find the ending stocks
in million barrels for each report.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys
from dateutil.relativedelta import relativedelta

url = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WGTSTUS1&f=W'
site_contents = urllib2.urlopen(url).read() # open the url and store the contents    
startdate = datetime.datetime.now() # the startdate is today's date
num_days = 25
z = 0
while z < num_days:
    date = startdate - datetime.timedelta(days = z) # subtracts one day from the start date each iteration
    [ month, year ] = [ date.strftime('%b'), date.strftime('%Y') ] # stores the month in mmm format and the year in YYYY format
    date_begin = site_contents.find( year + '-' + month ) # find the date in YYYY-mmm format in the report
    date_cutoff = date + relativedelta(months = 1) # add one month to the date to get the cutoff date
    [ end_month, end_year ] = [ date_cutoff.strftime('%b'), date_cutoff.strftime('%Y') ]
    date_end = site_contents.find(end_year + '-' + end_month, date_begin) # find the cutoff date in YYYY-mmm format
    [ month, day ] = [str(date.strftime('%m')), str(date.strftime('%d'))] # find the month in mm format and the day in dd format of 'date'
    data_begin = site_contents.find(month + '/' + day, date_begin, date_end) # find where the data begins between date_begin and date_end
    if data_begin != -1: # if data_begin isn't -1 (report date is valid)
        value_end = site_contents.find('&nbsp;', data_begin + 7) # store the index of where the value ends
        value_begin = site_contents.rfind('>', data_begin, value_end) # store the index of where the value begins
        value = float(site_contents[value_begin+1:value_end].replace(',','')) / 1000 # store value and divide by 1000 to convert from thousand barrels to million barrels
        headings = ['Date', 'Stocks (Million Barrels)']
        data = {'Date': [date.strftime('%Y%m%d')], 'Stocks (Million Barrels)': [value]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'WEEKLY_US_MOTOR_GAS_STOCKS\r'# build unique quandl code
        reference_text = '  Historical figures from EIA can be verified' \
        '\n  at http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WGTSTUS1&f=W'
        print 'code: ' + quandl_code + '\n'
        print 'name: Weekly US Motor Gasoline Stocks \n'
        print 'description: Weekly stocks of finished motor gasoline in million barrels'\
        '\n  for the United States. \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.eia.gov/dnav/pet/pet_stoc_wstk_dcu_nus_w.htm\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
    z = z + 1