# -*- coding: utf-8 -*-
"""
Created on Tue Jul 08 10:45:38 2014

@author: nataliecmoore

Script Name: USD_TO_MXN_SCRAPER

Purpose:
Find the daily conversion rate between USD and MXN

Approach:
Accessed the oanda conversion table and used string parsing to find the
conversion rate. 

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys

date = datetime.datetime.now().strftime('%m/%d/%y') # store today's date in the form mm/dd/YYYY
url = 'http://www.oanda.com/currency/table?date='+date+'&date_fmt=us&exch=MXN&sel_list=USD&value=1&format=CSV&redirected=1'
site_contents = urllib2.urlopen(url).read() # open the url and store the contents

beginning = site_contents.find(',', site_contents.find('USD', site_contents.find('\n\nUS Dollar'))) # find the ',' before the value begins
end = site_contents.find(',', site_contents.find('.', beginning)) # find the ',' after the value ends
value = site_contents[beginning+1:end].strip().replace('</span>','') # store the value and remove html tags

headings = ['Date', 'MXN/1 USD']
data = {'Date': [date], 'MXN/1 USD': [value]}
data_df = pd.DataFrame(data, columns = headings)
data_df.index = data_df['Date']
data_df = data_df.drop('Date', 1)
quandl_code = 'USD_TO_MXN\r'# build unique quandl code
#reference_text = '  Historical figures can be verified' \
#'\n  at http://markets.ft.com/research//Tearsheets/PriceHistoryPopup?symbol=ICX:CME\n' 
print 'code: ' + quandl_code + '\n'
print 'name: USD to MXN Exchange Rate \n'
print 'description: Daily exchange rate in pesos for one US dollar. \n'
print 'reference_url: http://www.oanda.com/currency/converter/ \n'
print 'frequency: daily\n'
print 'private: false\n'
print '---\n'
data_df.to_csv(sys.stdout)
print '\n'
print '\n'