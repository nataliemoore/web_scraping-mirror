# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 08:28:15 2014

@author: nataliecmoore

Script Name: USDA_NW_PY002_HIST_SCRAPER

Purpose:
Retrieve weekly national broiler/fryer data for the past 2 weeks from the 
NW_PY002 report via the USDA for upload to Quandl.com. The script pulls data 
for head and average live weight for each weight category tracked by USDA.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past 2 weeks. Iterated through each date and then used string indexing to 
obtain the data for each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/20/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import datetime
import urllib2
import sys
import re


num_days = 21 # number of previous days to find data for
# Iterates through each date until a Saturday is found (the ending day of the 
# week for the report). Then it loops through each weight category and finds 
# the data associated with each weight. Finally a table is created to hold the 
# data for each report and it is formatted for upload to Quandl.com 
x = 7
while x <= num_days:
    startdate = datetime.datetime.now() - datetime.timedelta(days=x) # subtracts one day from today's date each iteration    
    if startdate.weekday() != 5: # if the date isn't a Saturday, increment x to try the next date
        x = x + 1
    else:
        report_date = startdate + datetime.timedelta(days=5) # The report date is five days after the week ending date  
        month = str(report_date.month) # store the month as a string
        if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
            month = '0' + month # prepend a zero to the month
        string_date = report_date.strftime('%Y%m%d') # store the report date in the string form YYYYmmdd
        """
        Some dates in the AMS archive don't work correctly (because the incorrect report
        was uploaded, etc.) If that occurs, uncomment the following code and replace 
        '20140428' with the date that isn't working (type >>>string_date into the console)
        and then replace '20140425' with the date of the previous business day.

        if string_date == '20140428':
           string_date = '20140425'
           
        """ 
        # This try/except block accounts for reports that are missing from the AMS
        # archive because the USDA was closed during that day and didn't publish 
        # reports (Such as during a government shutdown). 
        try:
            # Find the url of the archived report for 'date' and then store the 
            # contents in site_contents. 
            # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
            # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
            # in AA_AA### format.
            target_url = 'http://search.ams.usda.gov/mndms/' + str(report_date.year) + '/' + month + '/NW_PY002' + string_date + '.TXT'
            site_contents = urllib2.urlopen(target_url).read()
        except urllib2.HTTPError: # This error occurs when a report is missing
            x = x + 1 # increment x to skip that day and go to the next
            continue # exit current iteration and go to the next
        labels = [ 'Head', 'Avg Live Wgt' ]
        weight_labels = [ '4.25 lbs & down', '4.26-6.25 lbs', '6.26-7.75 lbs', '7.76 lbs & up', 'Total' ]
        lines = []
        # Loops through each label and adds the data for each in "lines"
        z = 0
        while z < len(labels):
            start = site_contents.find(labels[z]) # stores index of the beginning of the label
            end = site_contents.find('\r\n', start) # stores index of the end of the data line
            line = site_contents[start:end].split('  ') # create a list that splits the data line according to '  '
            line = [l for l in line[1:] if len(l) != 0] # removes empty entries and label name from the list
            line = [float(l.replace(',','')) for l in line] # removes commas and converts values to floats
            lines.append(line)
            z = z + 1
        # Loops through each weight label and creates a table with the head and average live weight
        # values for each.
        y = 0
        while y < len(weight_labels):
            headings = [ 'Date', 'Head', 'Average Live Weight' ]
            data = { 'Date': [startdate.strftime('%Y%m%d')], 'Head': [lines[0][y]], 'Average Live Weight': [lines[1][y]] }
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            replace = re.compile('[ /-]') # list of characters to be replaced in the pork cut description
            remove = re.compile('[,%#&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
            name1 = replace.sub('_', weight_labels[y]) # replace certain characters with '_'
            name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
            quandl_code = 'USDA_NW_PY002_' + name2 + '\r'
            print 'code: ' + quandl_code
            print 'name: Weekly National Chicken Slaughter- ' + weight_labels[y].title() + '\r'
            reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
            '\n  at http://mpr.datamart.ams.usda.gov.\n' 
            print 'description: Weekly head and average live weight of broiler/fryer chickens ' \
            '\n  from the USDA NW_PY002 report published by the USDA Agricultural Marketing Service ' \
            '\n  (AMS). This dataset covers ' + weight_labels[y].lower() + '.\n'\
            + reference_text    
            print 'reference_url: http://www.ams.usda.gov/mnreports/nw_py002.txt'
            print 'frequency: daily'
            print 'private: false'
            print '---'
            data_df.to_csv(sys.stdout)
            print ''
            print ''
            y = y + 1
        x = x + 1