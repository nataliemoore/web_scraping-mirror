# -*- coding: utf-8 -*-
"""
Created on Thu Jun 19 12:58:20 2014

@author: nataliecmoore

Script Name: USDA_LM_HG210_HIST_SCRAPER_v2

Purpose:
Retrieve daily Eastern Cornbelt USDA data from the LM_HG210 report for the past 
5 days via the USDA AMS web service for upload to Quandl.com. The script pulls 
data for the base price range and the weighted average base price for slaughtered hogs.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the base price range and weighted average base price for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/19/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 150 # number of previous business days to find data for
startdate = datetime.datetime.now()-13*150*pto.BDay() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the data is located and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtracts one business day from the start date each iteration
    month = str(date.month)  # store month in string format
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       x = x + 1
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown).
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_HG210' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x = x + 1 # increment x to skip that day and go to the next
        continue
    starting_index = site_contents.find('NEGOTIATED PURCHASE') # store the index of the beginning of the section
    ending_index = site_contents.find('--', starting_index) # store the index of the end of the section
    # If no prices are reported store 0 for each value.
    # More recent reports say "Price not reported due to confidentiality" and earlier reports say:
    # "Due to confidentiality..." so the phrase "ue to confidentiality" is used because it overlaps
    # with both phrases.
    if site_contents.find('ue to confidentiality', starting_index, ending_index) != -1:
        base_price = [0, 0, 0]
    # if prices are reported use indexing to find and store the relevant values
    else:
        base_price_index = site_contents.rfind('Base Price Range', 0, ending_index) # store the index of the beginning of the base price section
        base_price_start = site_contents.find('$', base_price_index) # store the index of where the base price data value begins
        base_price_end = site_contents.find(',', base_price_start) # store the index of where the base data value ends
        base_price = site_contents[base_price_start+1:base_price_end].split('-') # split the value into min and max value and store in list
        base_price = [float(y.replace('$', '').replace('*','')) for y in base_price] # remove $ and convert values to floats
        weighted_average_index = site_contents.rfind('Weighted Average', 0, ending_index) # store the index of the beginning of the weighted average section
        weighted_average_start = site_contents.find('$', weighted_average_index) # find where the weighted average value begins
        weighted_average_end = site_contents.find('\r\n', weighted_average_start) # find end of weighted average value
        base_price.append(float(site_contents[weighted_average_start+1:weighted_average_end].replace('*',''))) # add weighted average to base_price and convert to float
    headings = [ 'Date', 'Minimum Base Price', 'Maximum Base Price', 'Weighted Average' ]
    data ={ 'Date': [string_date], 'Minimum Base Price': [base_price[0]], 'Maximum Base Price': [base_price[1]], \
      'Weighted Average': [base_price[2]] }
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_LM_HG210_EASTERN_BASE_PRICE\r'
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' 
    print 'code: ' + quandl_code+'\n'
    print 'name: Eastern Cornbelt Hog Report- Negotiated Purchase Base Price \n'
    print 'description: Eastern Cornbelt daily negotiated purchase base price including minimum/maximum base price and weighted average ' \
    'from the USDA LM_HG210 report published by the USDA Agricultural Marketing Service ' \
    '(AMS).  \n'\
    + reference_text + '\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg210.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x = x + 1