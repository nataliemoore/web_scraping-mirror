# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 15:23:18 2014

@author: nataliecmoore

Script Name: EIA_US_PROPANE_STOCKS_SCRAPER

Purpose:
Find the most recent ending stocks of propane and propylene by accessing the 
report published by the U.S. Energy Information Administration.

Approach:
Used string parsing to find the ending stocks value for the most recent report 
and then formmated for upload to quandl.com

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys

url = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WPRSTUS1&f=W'
site_contents = urllib2.urlopen(url).read() # open the url and store the contents
startdate = datetime.datetime.now() # the start date is today's date
success = 0 # initalize success to 0
# Loops by subtracting one day each iteration until a valid report date is found.
x = 0
while success != 1:
    date = startdate - datetime.timedelta(days = x) # subtract one day from the start date each iteration
    [ month, year ] = [ date.strftime('%b'), date.strftime('%Y') ] # store the month in mmm format and the year in YYYY format
    date_begin = site_contents.find(year + '-' + month) # find the index of the date in YYYY-mmm format
    [ month, day ] = [ str(date.strftime('%m')), str(date.strftime('%d')) ] # find the month in mm format and the day in dd format
    data_begin = site_contents.find(month + '/' + day, date_begin) # index where data begins
    if data_begin == -1: # data_begin is -1 if the date isn't a report date
        x = x + 1 # increment x to go to next date on next iteration
    else:
        success = 1 # set success to 1 to exit loop
        value_end = site_contents.find('&nbsp;', data_begin + 7) # index where value ends
        value_begin = site_contents.rfind('>', data_begin, value_end) # index where value begins
        value = float(site_contents[value_begin + 1:value_end].replace(',','')) / 1000 # store value and divide by 1000 to convert from thousand barrels to million barrels
        headings = ['Date', 'Propane/Propylene Stocks (Million Barrels)']
        data = {'Date': [date.strftime('%Y%m%d')], 'Propane/Propylene Stocks (Million Barrels)': [value]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'WEEKLY_US_PROPANE_STOCKS\r'# build unique quandl code
        reference_text = '  Historical figures from EIA can be verified' \
        '\n  at http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WPRSTUS1&f=W\n' 
        print 'code: ' + quandl_code + '\n'
        print 'name: Weekly US Propane/Propylene Stocks \n'
        print 'description: Weekly stocks of propane/propylene in million barrels'\
        '\n  for the United States. \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.eia.gov/dnav/pet/pet_stoc_wstk_dcu_nus_w.htm\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'