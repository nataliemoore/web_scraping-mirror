# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 14:59:40 2014

@author: nataliecmoore

Script Name: EIA_US_CRUDE_OIL_STOCKS_HIST_SCRAPER

Purpose: 
Find the U.S. ending stocks of crude oil for the past 3 weeks by accessing
the report published by the U.S. Energy Information Administration.

Approach:
Use string parsing to find the past 3 valid dates and then to find the data
associated with each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""

import urllib2
import datetime
import pandas as pd
import sys
from dateutil.relativedelta import relativedelta

url = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W'
site_contents = urllib2.urlopen(url).read() # open the url and store the contents in site_contents    
startdate = datetime.datetime.now() # the start date is today's date
num_days = 25 # the number of previous days to find data for
# Loops through each day and checks to see if it is a valid report date. A quandl data table
# is created for each of the valid days.
z = 0 
while z < num_days:
    date = startdate - datetime.timedelta(days = z) # subtracts one day from the start date each iteration
    [ month, year ] = [ date.strftime('%b'), date.strftime('%Y') ] # stores the 3 letter month and the 4 digit year
    date_begin = site_contents.find( year + '-' + month ) # find where the date in YYYY-mmm format begins
    date_cutoff = date + relativedelta(months = 1) # date_cutoff is the next month
    [ end_month, end_year ] = [ date_cutoff.strftime('%b'), date_cutoff.strftime('%Y') ] # store date_cutoff's month and year
    date_end = site_contents.find(end_year + '-' + end_month, date_begin) # find where the date_cutoff in YYYY-mmm format begins
    [ month, day ] = [str(date.strftime('%m')), str(date.strftime('%d'))] # stores the 2 digit month and 2 digit day for date
    data_begin = site_contents.find(month + '/' + day, date_begin, date_end) # find where the month and day begin between date_begin and date_end
    if data_begin != -1: # if date_begin isn't -1, then the date is a valid report date
        value_end = site_contents.find('&nbsp;', data_begin + 7) # stores the index where the data value ends
        value_begin = site_contents.rfind('>', data_begin, value_end) # stores the index of where the data value begins
        value = float(site_contents[value_begin+1:value_end].replace(',','')) / 1000 # store the value and divide by 1000 to convert thousand barrels to million barrels
        headings = ['Date', 'Crude Oil Stocks (Million Barrels)']
        data = {'Date': [date.strftime('%Y%m%d')], 'Crude Oil Stocks (Million Barrels)': [value]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'WEEKLY_US_CRUDE_OIL_STOCKS\r' # build unique quandl code
        reference_text = '  Historical figures from EIA can be verified' \
        '\n  at http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W\n' 
        print 'code: ' + quandl_code + '\n'
        print 'name: Weekly US Crude Oil Stocks \n'
        print 'description: Weekly stocks of crude oil in million barrels'\
        '\n  for the United States. \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
    z = z + 1