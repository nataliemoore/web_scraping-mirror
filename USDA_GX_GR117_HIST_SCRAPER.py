# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 15:50:37 2014

@author: nataliecmoore

Script Name: USDA_GX_GR117_HIST_SCRAPER

Purpose:
Retrieve data for the Central Illinois Soymeal high and low offer
price for the past 5 days using the USDA AMS web service.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the high and low offer price for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re

num_days = 5 # number of business days to find data for
startdate = datetime.datetime.now() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the min and max bids are listed for each soybean product and a table is created 
# to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day from the start date each iteration
    month = str(date.month) # store them month in string format   
    if len(month) == 1: # if month is Jan..Sept
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store the date in YYYYmmdd format
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       x = x + 1
       continue
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/GX_GR117' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # error that occurs when a report is missing
        x = x + 1 # increment x to skip that day and go to the next
        continue
    labels = ['Crude Soybean Oil', '48% Soybean Meal R', '48% Soybean Meal T', 'Soybean Hulls-bulk'] #names of each soybean product
    price_labels=['Cents/lb', '$/ton', '$/ton', '$/ton']    
    y = 0
    # Loops though the relevant section of the website and finds the offer data
    # for each soybean product in "labels"
    while y < len(labels):
        first_index = site_contents.find(labels[y]) # find the index of the label
        end_index = site_contents.find(' ', site_contents.find('-', first_index+len(labels[y])))
        if end_index>site_contents.find('Soybeans', first_index):
            line=[0, 0]
        else:
            line = (site_contents[first_index+len(labels[y]):end_index]).strip().split('-') # splits the data so that the minimum offer price and maximum
                                                                                            # offer price are stored in different sections in "line"
        headings = [ 'Date', 'Low Offer Price', 'High Offer Price']
        data = {'Date': [string_date], 'Low Offer Price': [line[0]], 'High Offer Price': [line[1]]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
        remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
        name1 = replace.sub('_', labels[y]) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        quandl_code = 'USDA_GX_GR117_' + name2 + '\n' # build unique quandl code
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' \
        '  Pricing is '+price_labels[y]+'\n'
        print 'code: ' + quandl_code + '\n'
        print 'name: Central Illinois Soybean Processor Report- ' + labels[y]
        print 'description: This dataset contains the Central Illinois Soybean Processor Report for ' + labels[y] + \
        '\n  from the USDA GX_GR117 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/gx_gr117.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1
    x = x + 1