# -*- coding: utf-8 -*-
"""
Created on Mon Jun 16 13:42:01 2014

@author: nataliecmoore

Script Name: USDA_NW_PY029_SCRAPER

Purpose:
Retrieve daily national turkey parts data from the NW_PY029 report via the USDA
for upload to Quandl.com. The script pulls data for volume and weighted average price
for each turkey cut tracked by the USDA.

Approach:
Split the website data into lines starting where the name of the cut began and
ending at the new line character. Then created a table for each cut to hold
the weighted average price and volume formatted for upload to quandl.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/16/2014      Natalie Moore   Initial development/release

"""

from pyparsing import Literal, Word, nums, printables
import re
import urllib2
import pandas as pd
import sys
import datetime

# stores report in variable "site_contents"
url = 'http://www.ams.usda.gov/mnreports/nw_py029.txt'
site_contents = urllib2.urlopen(url).read()
begin_date=site_contents.find('.', site_contents.find('IA'))+1
end_date=site_contents.find('  ', site_contents.find(',', begin_date))
# This try except block is used to find the date of the report. Because some reports use the three character
# abbreviated month name and others use the full month name, this block will account for both formats.
try:
    date=datetime.datetime.strptime(site_contents[begin_date:end_date].strip().replace(',',''), '%b %d %Y')
except ValueError:
    date=datetime.datetime.strptime(site_contents[begin_date:end_date].strip().replace(',',''), '%B %d %Y')

start = site_contents.find('\r\n', site_contents.find('(000)'))
while True:
    start_label = site_contents.find('\r\n', start + 1 ) + 2  
    start = start_label
    end_label1 = site_contents.find('  ', start_label)
    point_index = site_contents.rfind('.', 0, end_label1)
    end_label2 = site_contents.rfind(' ', 0, point_index)
    if point_index < start_label:
        end_label2 = end_label1
    end_label = min( [end_label1, end_label2] )
    ending_index = site_contents.find('\r\n', start_label) # index of new line character after label
    if site_contents.find("EXPORT TRADING") == ending_index + 2:
        break # finish iterating when export trading section begins
    empty_line = Literal(site_contents[start_label:end_label]) # empty line is only label with no values following
    nonempty_line = Literal(site_contents[start_label:end_label]) + Word(printables) + Word(nums+'.') + Word(nums) # non empty line has label with 3 data values following
    line_grammar = nonempty_line | empty_line # line can be either empty or non empty
    parsed = line_grammar.parseString(site_contents[start_label:ending_index]) 
    headings = [ 'Date', 'Weighted Average', 'Volume (000)' ]
    if len(parsed) != 1:
        data = { 'Date': [date.strftime('%Y-%m-%d')], 'Weighted Average': [parsed[2]], 'Volume (000)': [parsed[3]] }
    else:
        data = { 'Date': [date.strftime('%Y-%m-%d')], 'Weighted Average': [0], 'Volume (000)': [0] }
    name = site_contents[start_label:end_label]    
    if  name[len(name)-1] == '/': # remove the 1/, 2/, 3/ 4/ from end of names if needed
        name = name[:(len(name)-3)]
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    replace = re.compile('[ /-]') # list of characters to be replaced in the pork cut description
    remove = re.compile('[,%#&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
    name1 = replace.sub('_', name) # replace certain characters with '_'
    name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
    name2 = name2.translate(None, '-') # ensure '-' character is removed
    quandl_code = 'USDA_NW_PY029_'+name2+'\r'
    print 'code: ' + quandl_code
    print 'name: Daily National Turkey Parts- ' + name.title() + '\r'
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' 
    print 'description:  Weighted average price and volume of turkey parts' \
    '\n  from the USDA NW_PY029 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS). This dataset covers ' + name.lower() + '.\n'\
    + reference_text 
    print 'reference_url: http://www.ams.usda.gov/mnreports/nw_py029.txt'
    print 'frequency: daily'
    print 'private: false'
    print '---'
    data_df.to_csv(sys.stdout)
    print ''
    print ''
