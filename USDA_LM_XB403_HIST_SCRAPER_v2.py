# -*- coding: utf-8 -*-
"""
Created on Thu Jun 19 15:09:52 2014

@author: nataliecmoore

Script Name: USDA_LM_XB403_HIST_SCRAPER_v2

Purpose:
Retrieve the current cutout values of choice and select grade beef as well as
the weight, price range, and weighted average for fresh 50% lean trimmings for 
the past 5 days from the national daily boxed beef cutout report (USDA LM_XB403)

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing 
and pyparsing to obtain the weight, price range, and weighted average for each 
of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/19/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Literal, Word, nums
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 5 # number of previous business days to find data for
startdate = datetime.datetime.now() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the data is listed and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtracts one business day from the start date each iteration
    month = str(date.month) # stores the month as a string
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_XB403' + string_date+ '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x = x + 1 # increment x to skip that date and go to the next
        continue # skip to the next iteration of the loop
    name = Literal('Current Cutout Values:')
    line = name + Word(nums+'.'+'*') + Word(nums+'.'+'*') # grammar for current cutout values
    starting_index = site_contents.find('Current Cutout Values:')
    ending_index = site_contents.find('\r\n', starting_index)
    ccv = line.parseString(site_contents[starting_index:ending_index]) # parse line into its data components
    current_choice_value = ccv[1].replace('*','') # store choice value
    current_select_value = ccv[2].replace('*','') # store select value
    name = Literal('Fresh 50% lean trimmings')
    nonempty_line = name + Word(nums) + Word(nums+','+'.')*4
    empty_line_1 = name + Literal('0')*2
    empty_line_2 = name
    line = nonempty_line | empty_line_1 | empty_line_2 # grammar for lean trimmings values
    starting_index = site_contents.find('Fresh 50% lean trimmings') # start index of line of data
    ending_index = site_contents.find('\r\n', starting_index) # end index of line of data
    beef_trimmings = line.parseString(site_contents[starting_index:ending_index]) # parse line of data
    if len(beef_trimmings) == 6: # if the line of data isn't empty
        weight = float(beef_trimmings[2].replace(',', '')) # remove commas and convert weight to float
        min_price = float(beef_trimmings[3]) # store minimum price
        max_price = float(beef_trimmings[4]) # sore maximum price
        weighted_average = float(beef_trimmings[5]) # store weighted average
    else: # if the line of data is empty, store 0 for weight, min price, max price, and weighted average
        weight = 0
        min_price = 0
        max_price = 0
        weighted_average = 0
    # quandl data for current cutout values
    ccv_headings = [ 'Date', 'Choice Value', 'Select Value' ]
    ccv_data = { 'Date': [string_date], 'Choice Value': [current_choice_value], 'Select Value': [current_select_value] }
    ccv_df = pd.DataFrame(ccv_data, columns = ccv_headings)
    ccv_df.index = ccv_df['Date']
    ccv_df.drop(['Date'],inplace=True,axis=1) 
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' \
    '  All pricing is on a per CWT (100 lbs) basis.'
    print 'code: USDA_LM_XB403_CURRENT_CUTOUT_VALUES\n'
    print 'name: Daily Boxed Beef Cutout Values\n'
    print 'description: "Daily boxed beef cutout values from the USDA LM_XB403\n' \
    '  report published by the USDA Agricultural Marketing Service (AMS).\n' \
    + reference_text + '"\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_xb403.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    ccv_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    # quandl data for fresh 50% lean trimmings
    bt_headings = ['Date', 'Weight', '$ Min', '$ Max', 'Weighted Average']
    bt_data = {'Date': [string_date], 'Weight': [weight], '$ Min': [min_price], '$ Max': [max_price], 'Weighted Average': [weighted_average]}
    bt_df = pd.DataFrame(bt_data, columns = bt_headings)
    bt_df.index = bt_df['Date']
    bt_df.drop(['Date'],inplace=True,axis=1) 
    print 'code: USDA_LM_XB403_FRESH_50_LEAN_BEEF_TRIMMINGS\n'
    print 'name: Fresh 50% Lean Beef Trimmings\n'
    print 'description: "Daily values for fresh 50% lean beef trimmings from the USDA LM_XB403\n' \
    '  report published by the USDA Agricultural Marketing Service (AMS).\n' \
    + reference_text + '"\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_xb403.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    bt_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x = x + 1
