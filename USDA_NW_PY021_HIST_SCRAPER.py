# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 10:02:16 2014

@author: nataliecmoore

Script Name: USDA_NW_PY021_HIST_SCRAPER

Purpose:
Retrieve weekly national turkey slaughter data for the past 2 weeks from the 
NW_PY021 report via the USDA for upload to Quandl.com. The script pulls data 
for head and average live weight for each region tracked by USDA.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing and
pyparsing to find the head and average live weight data for each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/20/2014      Natalie Moore   Initial development/release

"""

from pyparsing import Suppress, Literal, Word, nums, printables
import pandas as pd
import datetime
import urllib2
import sys

num_days = 21 # number of previous days to find data for
# Iterates through each date until a Saturday is found (the ending day of the 
# week for the report). Then it loops through each location and finds 
# the data associated with it. Finally a table is created to hold the 
# data for each report and it is formatted for upload to Quandl.com 
x = 7
while x <= num_days:
    startdate = datetime.datetime.now() - datetime.timedelta(days=x) # one day is subtracted from today's date each iteration
    if startdate.weekday() != 5: # if the date isn't a Saturday, increment x to try the next date
        x = x + 1
    else:
        report_date = startdate + datetime.timedelta(days=5) # The report date is five days after the week ending date  
        month = str(report_date.month) # store the month as a string
        if len(month) == 1:  # if month has a length 1 (month is Jan..Sept)
            month = '0' + month # prepend a zero to the month
        string_date = report_date.strftime('%Y%m%d') # store the date in string form YYYYmmdd
        """
        Some dates in the AMS archive don't work correctly (because the incorrect report
        was uploaded, etc.) If that occurs, uncomment the following code and replace 
        '20140428' with the date that isn't working (type >>>string_date into the console)
        and then replace '20140425' with the date of the previous business day.
    
        if string_date == '20140428':
           string_date = '20140425'
   
        """ 
        # This try/except block accounts for reports that are missing from the AMS
        # archive because the USDA was closed during that day and didn't publish 
        # reports (Such as during a government shutdown). 
        try:
            # Find the url of the archived report for 'date' and then store the 
            # contents in site_contents. 
            # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
            # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
            # in AA_AA### format.
            target_url = 'http://search.ams.usda.gov/mndms/' + str(report_date.year) + '/' + month + '/NW_PY021' + string_date + '.TXT'
            site_contents = urllib2.urlopen(target_url).read()
        except urllib2.HTTPError: # This error occurs when a report is missing
            x = x + 1 # increment x
            continue # go to the next iteration of the loop
        # list of each region in the report
        labels = [ 'North East', 'South Atlantic', 'North Central', 'South Central', 'West', 'U.S. total' ]
        # Loops through each region and uses pyparsing to find the head and average 
        # live weight for the turkeys slaughtered. 
        z = 0
        while z < len(labels):
            suppress = Suppress(Word(printables))
            line = Literal(labels[z]) + suppress * 4 + Word(nums+','+'*') + Word(nums+'.'+'*') # grammar for each line of data following a region
            first = site_contents.find(labels[z]) # index of label
            end = site_contents.find('\r\n', first) # index of end of the line
            line = line.parseString(site_contents[first:end]) # parse line and store in list "line"
            line = [float(y.replace(',','').replace('*','')) for y in line[1:]] # remove commas and convert to floats
            headings = [ 'Date','Actual Turkey Slaughter', 'Turkey Average Weight' ]
            data = { 'Date':[startdate.strftime('%Y%m%d')], 'Actual Turkey Slaughter': [line[0]], 'Turkey Average Weight': [line[1]] }
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            name = labels[z].replace(' ','_').replace('.','')
            quandl_code = 'USDA_NW_PY021_' + name.upper() + '\r'
            print 'code: ' + quandl_code
            print 'name: Weekly National Turkey Slaughter- ' + labels[z].title() + '\r'
            reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
            '\n  at http://mpr.datamart.ams.usda.gov.\n' 
            print 'description:  Weekly national turkey slaughter data' \
            '\n  from the USDA NW_PY021 report published by the USDA Agricultural Marketing Service ' \
            '\n  (AMS). This dataset covers the ' + labels[z] + '.\n'\
            + reference_text 
            print 'reference_url: http://www.ams.usda.gov/mnreports/nw_py021.txt'
            print 'frequency: daily'
            print 'private: false'
            print '---'
            data_df.to_csv(sys.stdout)
            print ''
            print ''
            z = z + 1
        x = x + 1
    