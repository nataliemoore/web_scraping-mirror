# -*- coding: utf-8 -*-
"""
Created on Thu Jul 10 13:51:08 2014

@author: nataliecmoore

Script Name: CATTLE_ON_FEED_SCRAPER

Purpose:
Retrieve data from the monthly cattle on feed report published by the NASS, 
Agricultual Statistics Board, and USDA. The script pulls data for the number
of cattle on feed, placed on feed, marketings, and other disappearance for
each state tracked by the report.

Approach:
Used loops to go through the report and find each section of data. Then used
pyparsing and string parsing to find the data and created a table formatted for
upload to quandl.com

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/10/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys
from pyparsing import Suppress, Literal, Word, nums, ZeroOrMore
from dateutil.relativedelta import relativedelta

startdate = datetime.datetime.now() # the startdate is today's date
# Subtracts one day each iteration and checks if that date is a valid report
# date. If it is, success is set to 1 and the loop is exited. If not, x is
# incremented and the loop repeats. 
x = 0
success = 0
while success != 1:
    date = startdate - datetime.timedelta(days = x) # subtracts one day each iteration
    [ day, month, year ] = [ str(date.day), str(date.month), str(date.year) ] # stores day, month, and year of date as strings
    if len(month) == 1: # if month is Jan..Sept
        month = '0' + month # prepend a 0 so month is two digits
    try: 
        # Checks to see if the date is a valid report date by trying to read the url using the date's attributes.
        # If the date isn't valid, the urlllib2.HTTPError will be thrown and the loop will repeat.
        url = 'http://usda.mannlib.cornell.edu/usda/current/CattOnFe/CattOnFe-' + month + '-' + day + '-' + year + '.txt'
        site_contents = urllib2.urlopen(url).read()
        success = 1 # will reach this point only if the date is valid, set success to 1 to exit the loop
    except urllib2.HTTPError:
        x = x + 1  
# List of the states listed in the report
state_labels = [ 'Arizona', 'California', 'Colorado', 'Idaho', 'Iowa', 'Kansas', 'Minnesota', \
       'Nebraska', 'Oklahoma', 'South Dakota', 'Texas', 'Washington', 'Other States' ] 
# List of the sections of data that need to be found
name_labels = [ 'Cattle on Feed', 'Cattle Placed on Feed', 'Marketed', 'Other Disappearance' ]
end = site_contents.find('Number of Cattle on Feed') # set to point at beginning of report (will be changed each iteration of following loop)
new_date = date - relativedelta(months = 1) # subtract one month because data is for previous month that report is published
# Loops through each name label and finds the data for each state in state_labels
n = 0
while n < len(name_labels):
    end = site_contents.find(name_labels[n], end) # store where name_label occurs (always after previous name_label)
    x = 0
    while x < len(state_labels):
        start = site_contents.find(state_labels[x], end) # find where the state name occurs 
        end = site_contents.find('\r\n', start) # end is changed to end of line
        # This is the grammar for each line of data. It starts with the name of the state and is followed by a varying number
        # of periods. Then five numbers of data follow the colon.
        line_grammar = Literal(state_labels[x]) + Suppress(ZeroOrMore(Literal('.'))) + Suppress(Literal(':')) + Word(nums+',') * 5
        parsed = line_grammar.parseString(site_contents[start:end])[3] # parse the line and only keep the fourth element because it contains most recent data
        headings = ['Date', 'Thousand Head'] 
        # The 'Cattle on Feed' data corresponds to the current month so 1 month is added to the date and
        # the year, month, and day are converted to strings         
        if n == 0:
            year = (new_date + relativedelta(months = 1)).year
            month = str((new_date + relativedelta(months = 1)).month)
            day = (new_date + relativedelta(months = 1)).day
        else:
            year = new_date.year
            month = str(new_date.month)
            day = new_date.day
        if len(month) == 1:
            month = '0' + month # prepend 0 to month if it is one digit
        data = {'Date': [ str(year) + str(month) + str(day) ], 'Thousand Head': [parsed]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'CATTLE_ON_FEED_' + state_labels[x].upper().replace(' ','_') + '_' + name_labels[n].upper().replace(' ', '_') + '\r'# build unique quandl code
        reference_text = '  Historical figures can be verified' \
        '\n  at http://usda.mannlib.cornell.edu/MannUsda/viewDocumentInfo.do?documentID=1020\n'         
        print 'code: ' + quandl_code + '\n'
        print 'name: ' + state_labels[x] + '- ' + name_labels[n] + '\n'
        print 'description: Monthly cattle data for ' + state_labels[x] + '. ' \
              '\n  All prices are in $/cwt. \n'\
              + reference_text + '\n'
        print 'reference_url: ' + url + '\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n' 
        print '\n'
        x = x + 1
    n = n + 1
    
# This section is used to find the cattle placed on feed for the weight categories:
# under 600 lbs, 600-699 lbs, 700-799 lbs, 800+ lbs, and the total.
state_labels = ['Colorado', 'Kansas', 'Nebraska', 'Texas', 'Other States'] # list of states in the report
end = site_contents.find('Placed on Feed by Weight Group') # set end to a value before the data starts
# Loops through each state and uses pyparsing to find the data for each of the weight categories.
x = 0
while x < len(state_labels):
    start = site_contents.find(state_labels[x], end) # find the index of where the state name begins
    end = site_contents.find('\r\n', start) # find the end of the line
    # The grammar for each line of data starts with the name of the state following by a varying number of periods.
    # Then 10 numbers follow a colon.
    line_grammar = Literal(state_labels[x]) + Suppress(ZeroOrMore(Literal('.'))) + Suppress(Literal(':')) \
                 + Word(nums+',') * 10
    parsed = line_grammar.parseString(site_contents[start:end]) # parses and stores the line of data
    headings = ['Date', 'Under 600 lbs', '600-699 lbs', '700-799 lbs', '800+ lbs', 'Total']
    # The line data alternates this year and previous year's data for that month. Only this year's data is needed
    # so the even indices of parsed are used.
    data = {'Date': [new_date.strftime('%Y%m%d')], 'Under 600 lbs': [parsed[2]], '600-699 lbs': [parsed[4]], \
          '700-799 lbs': [parsed[6]], '800+ lbs': [parsed[8]], 'Total': [parsed[10]]} 
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    quandl_code = 'CATTLE_ON_FEED_' + state_labels[x].upper().replace(' ','_') + '_PLACED_ON'
    reference_text = '  Historical figures can be verified' \
    '\n  at http://usda.mannlib.cornell.edu/MannUsda/viewDocumentInfo.do?documentID=1020\n'         
    print 'code: ' + quandl_code + '\n'
    print 'name: Cattle Placed on Feed by Weight Group- ' + state_labels[x] + '\n'
    print 'description: Monthly cattle data for ' + state_labels[x] + '. '\
    '\n  All prices are in $/cwt. \n'\
    + reference_text + '\n'
    print 'reference_url: ' + url + '\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n' 
    print '\n'
    x = x + 1


