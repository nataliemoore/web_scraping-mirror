# -*- coding: utf-8 -*-
"""
Created on Tue Jul 08 07:58:29 2014

@author: nataliecmoore

Script Name: EIA_US_FUEL_OIL_STOCKS_SCRAPER

Purpose:
Find the weekly U.S. ending stocks for distillate fuel oil from the most recent
report published by the U.S. Energy Information Administration. This script 
finds the ending stocks in millions of barrels for 0-15 ppm, 15-500 ppm, and 
500+ ppm of sulfur.

Approach:
Loop through each url and find the most recent report. Then use pyparsing to 
extract the data value and create a table formatted for upload to quandl.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys

url1 = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WDISTUS1&f=W'
url2 = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WD0ST_NUS_1&f=W'
url3 = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WD1ST_NUS_1&f=W'
url4 = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WDGSTUS1&f=W'
code_labels = [ '_TOTAL', '_TOTAL_0_15', '_TOTAL_15_500', '_TOTAL_500_UP' ] # store the quandl code labels
name_labels = [ '', '- 0-15ppm Sulfur', '- 15-500 ppm Sulfur', '- 500+ ppm Sulfur' ] # store the quandl name labels
url_list = [ url1, url2, url3, url4 ] # create a list of each url
startdate = datetime.datetime.now() # the start date is today's date
# Loops through each url in url_list and then looks for the most recent report
x = 0
while x < len(url_list):
    site_contents = urllib2.urlopen(url_list[x]).read()
    success = 0 # store the contents of the current url
    # Loops until a valid date is found (success is set to 1)
    z = 0
    while success != 1:
        date = startdate - datetime.timedelta(days = z) # subtract one day from the start date each iteration
        [ month, year ] = [ date.strftime('%b'), date.strftime('%Y') ] # stores the month in mmm format and the year in YYYY format
        date_begin = site_contents.find(year + '-' + month) # find where the date in YYYY-mmmm occurs in the report
        [ month, day ] = [ str(date.strftime('%m')), str(date.strftime('%d')) ] # store the month in mm format and day in dd format
        data_begin = site_contents.find(month + '/' + day, date_begin) # find where the date occurs after date_begin
        if data_begin == -1: # if data_begin is -1
            z = z + 1 # increment z to go to next date on next iteration
        else:
            success = 1 # set success to 1
            value_end = site_contents.find('&nbsp;', data_begin + 7) # find where the data value ends
            value_begin = site_contents.rfind('>', data_begin, value_end) # find where the data value begins
            value = float(site_contents[value_begin + 1:value_end].replace(',','')) / 1000 # store the value and divide by 1000 to convert from thousand barrels to million barrels
            headings = [ 'Date', 'Stocks (Million Barrels)' ]
            data = {'Date': [date.strftime('%Y%m%d')], 'Stocks (Million Barrels)': [value]}
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            quandl_code = 'WEEKLY_US_FUEL_OIL_STOCKS' + code_labels[x] + '\r'# build unique quandl code
            reference_text = '  Historical figures from EIA can be verified' \
            '\n  at ' + url_list[x] + '\n' 
            print 'code: ' + quandl_code + '\n'
            print 'name: Weekly US Distillate Fuel Oil Stocks' + name_labels[x] + '\n'
            print 'description: Weekly stocks of distillate fuel oil in million barrels'\
            '\n  for the United States. \n' \
            + reference_text + '\n'
            print 'reference_url: http://www.eia.gov/dnav/pet/pet_stoc_wstk_dcu_nus_m.htm\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
    x = x + 1