# -*- coding: utf-8 -*-
"""
Created on Tue Jul 08 11:23:44 2014

@author: nataliecmoore

Script Name: USDA_BROILER_TYPE_SCRAPER

Purpose:
Retrieve weekly broiler-type eggs set and chicks placed data from the broiler 
hatchery report released by NASS, USDA, and the Agricultural Statistics Board.

Approach:
Looped through the past 7 days to find the date the report was published. Then
used string parsing to find the eggs set and chicks placed data and looped
through each piece of data to create a table formatted for upload to quandl.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys


start_date = datetime.datetime.now() # the start date is today's date
# Loops through the past 7 days to find the date the report was uploaded.
# This is needed because not all reports are published on the same day of the week;
# holidays may shift the day of the week forward or back from the normal upload day.
x = 0
while x <= 7:
    date = start_date - datetime.timedelta(days = x) # subtract one day from the start date each iteration
    year = str(date.year) # store the year as a string instead of an integer
    day = str(date.day) # store the day as a string
    month = str(date.month) # store the month as a string
    if len(month) == 1: # if the month is Jan..Sep
        month = '0' + month # prepend a zero so the month is 2 digits
    if len(day) == 1: # if the day is 1..9
        day = '0' + day # prepend a zero so the day is 2 digits
    # This try/except block checks if the day is a valid report date. If it is,
    # the contents of the website are stored. If not, the error is caught and 
    # the loop goes to the next iteration to try the next date.
    try:
        url = 'http://usda.mannlib.cornell.edu/usda/current/BroiHatc/BroiHatc-'+month+'-'+day+'-'+year+'.txt'
        site_contents = urllib2.urlopen(url).read()
        break # exit the loop
    except urllib2.HTTPError:
        x = x + 1
        continue # go to next loop iteration

beginning = site_contents.find("set", site_contents.find('Broiler-Type Eggs Set')) # store index of beginning of data 
end = site_contents.find('million', beginning) # store index of end of data
# store eggs set data, remove spaces, and multiply by 1000 so data is in thousands (in millions on website)
eggs_set = float(site_contents[beginning + 3:end].strip()) * 1000 

beginning = site_contents.find("placed", site_contents.find("Broiler-Type Chicks Placed")) # store index of beginning of data
end = site_contents.find('million', beginning) # store index of end of data
# store the number of chicks placed, remove spaces, and multiply by 1000 to convert data from millions to thousands
chicks_placed = float(site_contents[beginning + 6:end].strip()) * 1000

values = [eggs_set, chicks_placed] # place both values in a list
codes = ['EGGS_SET', 'CHICKS_PLACED'] # list of quandl code endings for each data set
names = ['Thousand Broiler Eggs Set', 'Thousand Chicks Placed'] # list of quandl names for each data set
# Loops through the eggs set and the chicks placed values and creates a quandl table
# for each. 
x = 0
while x < len(values):
    headings = ['Date', names[x]]
    data = {'Date': [date.strftime('%Y%m%d')], names[x]: [values[x]]}
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df['Date']
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_BROILER_'+codes[x]+'\r'# build unique quandl code
    reference_text = '  Historical figures can be verified' \
    '\n  at http://usda.mannlib.cornell.edu/MannUsda/viewDocumentInfo.do?documentID=1010' 
    print 'code: ' + quandl_code +'\n'
    print 'name: Weekly ' + names[x] + '\n'
    print 'description: Weekly ' + names[x].lower() + \
    '\n  for the United States. \n' \
    + reference_text + '\n'
    print 'reference_url: ' + url + '\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x = x + 1



