# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 10:46:19 2014

@author: nataliecmoore

Script Name: EIA_STOR_WK_SCRAPER

Purpose:
Find the amount of natural gas in underground storage for the previous week
for the regions: eastern, lower 48 states, producting region, and western
region.

Approach:
Looped through the reports for each region and found the most recent storage
reports published through the Energy Information Administration.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""

import urllib2
import datetime
import pandas as pd
import sys
# Creates a list to store the 4 urls corresponding to the underground storage data
# for each of the four regions
url1 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R88_BCF.W'
url2 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R48_BCF.W'
url3 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R87_BCF.W'
url4 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R89_BCF.W'
url_list = [url1, url2, url3, url4]
region_list = ['Eastern Region', 'Lower 48 States', 'Producing Region', 'Western Region']
# Loops through each url in url_list and finds the data for the most recent report
u = 0
while u < len(url_list):
    site_contents = urllib2.urlopen(url_list[u]).read() # reads the url and stores the contents
    startdate = datetime.datetime.now() # The startdate is today's date
    success = 0 # initialize 'success' to 0
    x = 0
    # Loops to find the most recent report by subtracting one day from today's date
    # each iteration. When a valid report date is found, success is set to 0 and the loop is exited
    while success != 1:
        date = startdate - datetime.timedelta(days = x) # subtracts one day from today's date each iteration
        year = str(date.year) # stores the year as a string
        # One month is subtracted from the current month to convert a month in the normal rnage 1..12 to the range
        # 0..11. The month is stored in the report with 0 corresponding to Jan, 1 corresponding to Feb, etc.
        month = str(date.month - 1)
        day = str(date.day) # stores the day as a string
        if len(day) == 1: # If the day is one digit
            day = '0'+day # prepend a 0 so it is 2 digits
        # Find the date in the report. It is stored in the format '[Date.UTC(YYYY, mm, dd)'
        date_begin = site_contents.find('[Date.UTC(' + year + ', ' + month + ', ' + day + '),')
        if date_begin == -1: # If the current date is not found in the report
            x = x + 1 # increment x
        else:
            date_end = site_contents.find('),', date_begin) # store index where the date ends
            value_end = site_contents.find(']', date_end) # store the index where the storage value ends
            value = site_contents[date_end+2:value_end] # store the storage value
            success = 1 # set success to 1 because the most recent report has been found
            headings = ['Date', 'Natural Gas (Billion Cubic Feet)']
            data = {'Date': [date.strftime('%Y-%m-%d')], 'Natural Gas (Billion Cubic Feet)': [value]}
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            quandl_code = 'WEEKLY_NAT_GAS_UNDERGROUND_STORAGE_' + region_list[u].replace(' ', '_').upper() + '\r'# build unique quandl code
            reference_text = '  Historical figures from EIA can be verified' \
            '\n  at http://www.eia.gov/beta/api/qb.cfm?category=371\n' 
            print 'code: ' + quandl_code + '\n'
            print 'name: Weekly Natural Gas Working Underground Storage- ' + region_list[u] + '\n'
            print 'description: Weekly amount of natural gas in underground storage'\
            '\n  for the ' + region_list[u] + '. \n'\
            + reference_text + '\n'
            print 'reference_url: http://www.eia.gov/dnav/ng/ng_stor_wkly_s1_w.htm\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
    u = u + 1
