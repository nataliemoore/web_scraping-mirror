# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 12:27:52 2014

@author: nataliecmoore

Script Name: USDA_LM_XB401_HIST_SCRAPER

Purpose:

Approach:

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/12/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re
from lxml import objectify

# Set the date range for the report (4 days to ensure all data is captured)
startdate = datetime.datetime.now() - 31 * pto.BDay()
startdate = startdate.strftime('%m/%d/%Y')

target_url = 'http://mpr.datamart.ams.usda.gov/ws/report/v1/beef/LM_XB401?filter={%22filters%22:[{%22fieldName%22:%22Report%20Date%22,%22operatorType%22:%22GREATER%22,%22values%22:[%2201/01/2014%22]}]}'

fileobj = urllib2.urlopen(target_url).read()
data=[]
root = objectify.fromstring(fileobj)
for report_date in root.report.iterchildren(): #processing must be repeated for each day covered by the report
    date = report_date.attrib['report_date']
    for report in report_date.iterchildren():
        if report.attrib['label']=='Central':
            new_report=report
            for item in new_report.iterchildren():
                if item.attrib['item_desc']=='Chemical Lean, Fresh  90%':
                    data.append([date, item.attrib['item_desc'], item.attrib['total_pounds'], \
                    item.attrib['price_range_low'], item.attrib['price_range_high'], item.attrib['price_range_avg']])
                if item.attrib['item_desc']=='Chemical Lean, Frozen 90%':
                    data.append([date, item.attrib['item_desc'], item.attrib['total_pounds'], \
                    item.attrib['price_range_low'], item.attrib['price_range_high'], item.attrib['price_range_avg']])
                if item.attrib['item_desc']=='Chemical Lean, Fresh  85%':
                    data.append([date, item.attrib['item_desc'], item.attrib['total_pounds'], \
                    item.attrib['price_range_low'], item.attrib['price_range_high'], item.attrib['price_range_avg']])
                    
                    
x=0
while x<len(data):
    headings = ['Date', 'Weight', '$ Min', '$ Max', 'Weighted Average']
    xb_data={'Date': [data[x][0]], 'Weight': [data[x][2]], '$ Min': [data[x][3]], \
    '$ Max': [data[x][4]], 'Weighted Average': [data[x][5]]}
    cl_df = pd.DataFrame(xb_data, columns = headings)
    cl_df.index = cl_df['Date']
    cl_df.drop(['Date'],inplace=True,axis=1) 
    replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
    remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
    name1 = replace.sub('_', re.sub(r'\s+', ' ', data[x][1])) # replace certain characters with '_'
    name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
    name2 = name2.translate(None, '-') # ensure '-' character is removed
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' \
    '  All pricing is on a per CWT (100 lbs) basis.'
    print 'code: USDA_LM_XB401_'+name2
    print 'name: Central Beef'+' - '+data[x][1]+' '+'\n'
    print 'description: "Daily values for central beef from the USDA LM_XB401\n' \
    '  report published by the USDA Agricultural Marketing Service (AMS).\n' \
    + reference_text + '"\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_xb401.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    cl_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x=x+1