# -*- coding: utf-8 -*-
"""
Created on Fri Jun 06 12:57:37 2014

@author: nataliecmoore

Script Name: USDA_LM_HG206_SCRAPER

Purpose:
Retrieve daily Iowa/Minnesota hog data from the LM_HG206 report via the USDA LMR
web service for upload to Quandl.com. The script pulls data for head count,
price range, and weighted average price.

Approach:
Used python string parsing to find each piece of data on the website.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/06/2014      Natalie Moore   Initial development/release

"""

import urllib2
import pandas as pd
import datetime
import sys

# stores report in variable 'site_contents'
url = 'http://www.ams.usda.gov/mnreports/lm_hg206.txt'
site_contents = urllib2.urlopen(url).read()
begin_date = site_contents.find(',', site_contents.find('Iowa'))+1
end_date = site_contents.find('  ', site_contents.find(',', begin_date))
# This try except block is used to find the date of the report. Because some reports use the three character
# abbreviated month name and others use the full month name, this block will account for both formats.
try:
    date = datetime.datetime.strptime(site_contents[begin_date:end_date].strip().replace(',',''), '%b %d %Y')
except ValueError:
    date = datetime.datetime.strptime(site_contents[begin_date:end_date].strip().replace(',',''), '%B %d %Y')

starting_index = site_contents.find(':', site_contents.find('Barrows & Gilts')) # stores index of colon before head count
end_head = site_contents.find('\r\n', starting_index) # stores index of the end of the head count
head = float(site_contents[starting_index+1:end_head].strip().replace(',','')) # store the head count, replace commas, and convert to a float
if site_contents.rfind('*Price not reported due to confidentiality*', starting_index, site_contents.find('IOWA/MINNESOTA DAILY DIRECT NEGOTIATED'))!=-1:
    low_price = 0 # store a zero for the low price
    high_price = 0 # store a zero for the high price
    weighted_average = 0 # store a zero for the weighted average
else:
    price_start = site_contents.find('$', site_contents.find('Base Price Range')) # store index of beginning of price range
    hyphen = site_contents.find('-', price_start) # store index of hyphen separating min and max price
    low_price = float(site_contents[price_start+1:hyphen].strip().replace('*','')) # store min price
    price_end = site_contents.find(',', hyphen) # store index of comma that marks end of price range
    high_price = float(site_contents[hyphen+1:price_end].strip().replace('$','').replace('*',''))
    weighted_average_begin = site_contents.find('$', price_end) # find index of $ before weighted average
    weighted_average_end = site_contents.find('\r\n', weighted_average_begin) # find index of line break after weighted average
    weighted_average = float(site_contents[weighted_average_begin+1:weighted_average_end].strip().replace('*','')) # store weighted average
if site_contents.rfind('*Price not reported due to confidentiality*', site_contents.find('Barrows & Gilts (live basis,'),site_contents.find('BARROWS & GILTS PURCHASE BY STATE OF ORIGIN'))!=-1:
    low_price2 = 0
    high_price2 = 0
    weighted_average2 = 0
else:
    price_start2 = site_contents.find('$', site_contents.find('Barrows & Gilts (live basis')) 
    hyphen2 = site_contents.find('-', price_start2) 
    low_price2 = float(site_contents[price_start2+1:hyphen2].strip().replace('*',''))  
    price_end2 = site_contents.find(',', hyphen2)
    high_price2 = float(site_contents[hyphen2+1:price_end2].strip().replace('$','').replace('*',''))
    weighted_average_begin2 = site_contents.find('$', price_end2) # find index of $ before weighted average
    weighted_average_end2 = site_contents.find('\r\n', weighted_average_begin2) # find index of line break after weighted average
    weighted_average2 = float(site_contents[weighted_average_begin2+1:weighted_average_end2].strip().replace('*','')) # store weighted average

headings = [ 'Date', 'Lean Hog Head Count', 'Lean Hog Low Price', 'Lean Hog High Price', 'Lean Hog Weighted Average Price', \
          'Live Hog Low Price', 'Live Hog High Price', 'Live Hog Weighted Average Price' ]
data={'Date': [date.strftime('%Y-%m-%d')], 'Lean Hog Head Count': [head], 'Lean Hog Low Price': [low_price], \
      'Lean Hog High Price': [high_price], 'Lean Hog Weighted Average Price': [weighted_average], 'Live Hog Low Price': [low_price2],\
      'Live Hog High Price': [high_price2], 'Live Hog Weighted Average Price': [weighted_average2]}
data_df = pd.DataFrame(data, columns = headings)
data_df.index = data_df['Date']
data_df = data_df.drop('Date', 1)
quandl_code = 'USDA_LM_HG206'  # build unique quandl code
reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
'\n  at http://mpr.datamart.ams.usda.gov.\n' \
'  All pricing is on a per CWT (100 lbs) basis.'
print 'code: ' + quandl_code
print 'name: Iowa/Minnesota Daily Direct Hog Report' 
print 'description: Daily head count, price range and weighted average price' \
'\n  from the USDA LM_HG206 report published by the USDA Agricultural Marketing Service ' \
'\n  (AMS).' + reference_text
print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg206.txt'
print 'frequency: daily'
print 'private: false'
print '---'
data_df.to_csv(sys.stdout)
print ''
print ''