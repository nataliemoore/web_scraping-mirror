# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 10:16:40 2014

@author: nataliecmoore

Script Name: USDA_AJ_PY047_HIST_SCRAPER

Purpose:
Retrieve northeast broiler/fryer data for the past 5 buiness days from the AJ_PY047 
report via the USDA AMS archive for upload to Quandl.com. The script pulls data for 
weighted average price and volume for each individual cut tracked by USDA.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used pyparsing to 
obtain the weighted average price and volume for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Literal, Word, nums
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re

num_days = 25 # number of previous business days to find data for
startdate = datetime.datetime.now()-68*25*pto.BDay() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then loops through each of the names of the cuts in the report and 
# creates a data table for each cut on each date.
x = 1
while x <= num_days: 
    date = startdate - x * pto.BDay() # subtracts one day from the start date each iteration
    month = str(date.month) # stores the month as a string
    if len(month) == 1: # if the month is Jan..Sep
        month = '0' + month # prepend a zero to make the month 2 digits
    string_date = date.strftime('%Y%m%d') # store the date in YYYYmmdd format
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
        string_date = '20140425'
    """
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    if string_date == '20090316':
        x=x+1
        continue
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of 
        # the report in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/AJ_PY047' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This is the error that occurs when a report is missing
        x = x + 1 # increment x to go to the next date
        continue
    # names of each cut in the report
    labels = [ 'BREAST - B/S', 'TENDERLOINS', 'BREAST - WITH RIBS', 'BREAST - LINE RUN', 'LEGS', 'LEG QUARTERS (BULK)',\
        'DRUMSTICKS', 'THIGHS', 'B/S THIGHS', 'WINGS (WHOLE)', 'BACKS AND NECKS (STRIPPED)', 'LIVERS (5 POUND TUBS)',\
        'GIZZARDS (HEARTS)' ]
        

    ending_index = 0 # initializes ending_index to 0 to be used in following loop       
    # Loops through each cut in labels and uses pyparsing to find the weighted average
    # and volume for that cut. The data and data are formatted into a table and the 
    # relevant quandl data is printed.
    y = 0
    while y < len(labels):
        starting_index = site_contents.find(labels[y], ending_index)
        ending_index = site_contents.find('\r\n', starting_index)
        line = Literal(labels[y]) + (Word(nums+'-'+'*') | Literal('TFEWR')) + (Word(nums+'.'+'*') | Literal('TFEWR')) + (Word(nums+','+'*') | Literal('TFEWR')) # grammar to find each label's data
        text = site_contents[starting_index:ending_index] # the line to be parsed is from starting_index to ending_index
        parsed = line.parseString(text) # parses the line and stores it in "parsed"
        parsed = [p.replace('TFEWR', '0').replace('*','') for p in parsed]        
        headings = ['Date', 'Weighted Average (Price)', 'Volume (Lbs)']
        data = {'Date': [string_date], 'Weighted Average (Price)': [parsed[2]], 'Volume (Lbs)': [parsed[3].replace(',','')]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        replace = re.compile('[ /]') # list of characters to be replaced 
        remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed
        name1 = replace.sub('_', labels[y]) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        quandl_code = 'USDA_AJ_PY047_' + name2 + '\r'
        print 'code: ' + quandl_code
        print 'name: Daily Northeast Broiler/Fryer Parts- ' + labels[y].title() + '\r'
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' 
        print 'description: Daily weighted average price and volume of northeast broiler/fryer parts ' \
        '\n  from the USDA AJ_PY047 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). This dataset covers ' + labels[y].lower() + '.\n'\
        + reference_text
        print 'reference_url: http://www.ams.usda.gov/mnreports/aj_py047.txt'
        print 'frequency: daily'
        print 'private: false'
        print '---'
        data_df.to_csv(sys.stdout)
        print ''
        print ''
        y = y + 1
    x = x + 1