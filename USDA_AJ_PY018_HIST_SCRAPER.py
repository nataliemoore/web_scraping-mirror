# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 09:45:49 2014

@author: nataliecmoore

Script Name: USDA_AJ_PY018_HIST_SCRAPER

Purpose:
Find the Georgia f.o.b. dock quoted price on broilers/fryers for the past 
5 business days

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the quoted price for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 50 # number of previous business days to find data for
startdate = datetime.datetime.now() -39*50*pto.BDay() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the quoted price is listed and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtracts one business day from the start date each iteration
    month = str(date.month) # store month in string format instead of integer    
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/AJ_PY018' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x = x + 1 # increment x to skip that day and go to the next
        continue    
    decimal = site_contents.rfind('.', 0, site_contents.find('based'))            # find the decimal point in the price
    space_before = site_contents.rfind(' ', 0, decimal)                           # find the space before the price
    space_after = site_contents.find(' ', decimal)                                # find the space after the price
    try:
        dock_quoted_price = float(site_contents[space_before:space_after].replace('*','').strip()) # store the quoted price as a float and remove extra spaces around it
    except ValueError:
        x=x+1
        continue
    headings = ['Date', 'Quoted Price (cents)']                                   # headings for the data
    data = {'Date': [string_date], 'Quoted Price (cents)': [dock_quoted_price]}   # store the data associated with each heading
    data_df = pd.DataFrame(data, columns = headings) # create a data frame for the data
    data_df.index = data_df['Date'] 
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_AJ_PY018_BROILER_FRYER_QUOTED_PRICE\r' # build unique quandl code
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' 
    print 'code: ' + quandl_code +'\n'
    print 'name: Georgia F.O.B. Dock Broiler/Fryer Quoted Price\n'
    print 'description: Georgia F.O.B. Dock quoted price on broilers/fryers'\
    '\n  from the USDA AJ_PY018 report published by the USDA Agricultural Marketing Service ' \
    '\n  (AMS).\n'\
    + reference_text +'\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/AJ_PY018.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x = x + 1
