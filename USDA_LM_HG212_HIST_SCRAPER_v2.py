# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 11:33:17 2014

@author: nataliecmoore

Script Name: USDA_LM_HG212_HIST_SCRAPER_v2

Purpose:
Retrieve daily Eastern Cornbelt USDA data from the LM_HG210 report via the USDA LMR
web service for upload to Quandl.com. The script pulls data for the base price 
range and the weighted average base price for slaughtered hogs.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the base price range and the weighted average base price\ for each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys

num_days = 5 # number of previous business days to find data for
startdate = datetime.datetime.now() # start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the section of the website where
# the data is located and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day from the startdate each iteration
    month = str(date.month) # store month in string format   
    if len(month) == 1: # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       x = x + 1
       string_date = '20140425'
   
    """ 
    try:
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_HG212' + string_date + '.TXT'
        site_contents=urllib2.urlopen(target_url).read()
    except urllib2.HTTPError:
        x=x+1
        continue
    start = site_contents.find('NEGOTIATED PURCHASE') # store the index of the beginning of the negotiated purchase section
    end = site_contents.find('--', start)     # store the index of the end of the negotiated purchase section
    if site_contents.rfind('Price not reported due to', start, end) != -1:
        base_price = [0, 0, 0]
    else:
        base_price_index = site_contents.rfind('Base Price Range', start, end) # store the index of the beginning of the base price section
        weighted_average_index = site_contents.rfind('Weighted Average', start, end) #store the index of the weighted average section
        base_price_start = site_contents.find('$', base_price_index) # store the index of where the base price values begin
        base_price_end = site_contents.find(',', base_price_index)   # store the index of where the base price values end
        weighted_average_start = site_contents.find('$', weighted_average_index) # store the index of the beginning of the w.a. value
        weighted_average_end = site_contents.find('\r\n', weighted_average_start) # store the index of the end of the w.a. value
        base_price = site_contents[base_price_start+1:base_price_end].split('-') # add the base price min and max range to "base_price"
        base_price = [float(b.replace('$', '').replace('*','').strip()) for b in base_price] # remove $ and convert value to float
        weighted_average = float(site_contents[weighted_average_start+1:weighted_average_end].replace('*','')) # find weighted average and convert to float
        base_price.append(weighted_average) # store weighted average in base_price
    headings = [ 'Date', 'Minimum Base Price', 'Maximum Base Price', 'Weighted Average' ]
    data = { 'Date': [string_date], 'Minimum Base Price': [base_price[0]], 'Maximum Base Price': [base_price[1]], \
      'Weighted Average': [base_price[2]] }
    data_df = pd.DataFrame(data, columns = headings)
    data_df.index = data_df[ 'Date' ]
    data_df = data_df.drop('Date', 1)
    quandl_code = 'USDA_LM_HG212_WESTERN_BASE_PRICE\r'
    reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
    '\n  at http://mpr.datamart.ams.usda.gov.\n' 
    print 'code: ' + quandl_code+'\n'
    print 'name: Western Cornbelt Hog Report- Negotiated Purchase Base Price \n'
    print 'description: Western Cornbelt daily negotiated purchase base price including minimum/maximum base price and weighted average ' \
    'from the USDA LM_HG212 report published by the USDA Agricultural Marketing Service ' \
    '(AMS).  \n'\
    + reference_text + '\n'
    print 'reference_url: http://www.ams.usda.gov/mnreports/lm_hg212.txt\n'
    print 'frequency: daily\n'
    print 'private: false\n'
    print '---\n'
    data_df.to_csv(sys.stdout)
    print '\n'
    print '\n'
    x = x + 1