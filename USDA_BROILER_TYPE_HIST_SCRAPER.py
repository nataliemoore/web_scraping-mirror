# -*- coding: utf-8 -*-
"""
Created on Tue Jul 08 12:45:28 2014

@author: nataliecmoore

Script Name: USDA_BROILER_TYPE_HIST_SCRAPER

Purpose:
Retrieve weekly broiler-type eggs set and chicks placed data from the broiler 
hatchery report released by NASS, USDA, and the Agricultural Statistics Board 
for the past 3 weeks

Approach:
Used string parsing to find the eggs set and chicks placed data and looped
through each piece of data to create a table formatted for upload to quandl.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/08/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys

start_date = datetime.datetime.now() # the start date is today's date
num_days = 25 # number of previous days to find data for
# Loops through each day and finds the data for each report
x = 0
while x < num_days:
    date = start_date - datetime.timedelta(days=x) # subtracts one day from the start date each iteration
    year = str(date.year) # stores the year as a string
    day = str(date.day)   # stores the day as a string 
    month = str(date.month) # stores the month as a string
    if len(month) == 1: # convert the 1 digit months to 2 digit
        month = '0' + month 
    if len(day) == 1: # convert the 1 digit days to 2 digit
        day = '0' + day
    try:
        url = 'http://usda.mannlib.cornell.edu/usda/nass/BroiHatc//' + year[:3] + '0s/' + year + '/BroiHatc-' + month + '-' + day + '-' + year + '.txt'
        site_contents = urllib2.urlopen(url).read() # open the url and store the contents
    except urllib2.HTTPError:
        x = x + 1 # increment x
        continue # continue to next iteration
    beginning = site_contents.find('set', site_contents.find('Eggs')) # the index of the word before the eggs set value
    end = site_contents.find('million', beginning) # the index of the word after the eggs set value
    eggs_set = float(site_contents[beginning+3:end].strip())*1000 # store the value and multiply by 1000 to convert from million to thousand

    beginning = site_contents.find("placed", site_contents.find("Chicks")) # the index of the word before the chicks placed value
    end = site_contents.find('million', beginning) # the index of the word after the chicks placed value
    chicks_placed = float(site_contents[beginning+6:end].strip())*1000 # store the value and multiply by 1000 to convert from million to thousand

    values = [eggs_set, chicks_placed] # store the eggs set and chicks placed values in a list
    codes = ['EGGS_SET', 'CHICKS_PLACED'] # list of quandl codes 
    names = ['Thousand Broiler Eggs Set', 'Thousand Chicks Placed'] # list of quandl names
    # Loops through each value in values and creates a quandl dataset for each
    y = 0
    while y < len(values):
        headings = ['Date', names[y]]
        data = {'Date': [date.strftime('%Y%m%d')], names[y]: [values[y]]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'USDA_BROILER_' + codes[y] + '\r'# build unique quandl code
        reference_text = '  Historical figures can be verified' \
        '\n  at http://usda.mannlib.cornell.edu/MannUsda/viewDocumentInfo.do?documentID=1010' 
        print 'code: ' + quandl_code + '\n'
        print 'name: Weekly '+names[y]+'\n'
        print 'description: Weekly ' + names[y].lower() + \
        '\n  for the United States. \n'\
        + reference_text +'\n'
        print 'reference_url: ' + url + '\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1
    x = x + 1

