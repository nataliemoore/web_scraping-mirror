# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 14:14:42 2014

@author: nataliecmoore

Script Name: EIA_US_CRUDE_OIL_STOCKS_SCRAPER

Purpose:
Find the most recent U.S. ending stocks of crude oil report by accessing
the report published by the U.S. Energy Information Administration.

Approach:
Use string parsing to find the past valid date and then to find the data
associated with each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""
import urllib2
import datetime
import pandas as pd
import sys

url = 'http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W'
site_contents = urllib2.urlopen(url).read() # open the url and store the contents in site_contents
startdate = datetime.datetime.now() # the start date is today's date
success = 0 # initialize success to 0
# Loops through previous days to find the most recent report published. When a 
# valid date is found, a quandl data table is created and success is set to 1
# to exit the loop.
x = 0
while success != 1:
    date = startdate - datetime.timedelta(days = x) # subtract one day from the start date each iteration
    [ month, year ] = [ date.strftime('%b'), date.strftime('%Y') ]  # stores the 3 letter month and the 4 digit year
    date_begin = site_contents.find(year + '-' + month) # find where the date in YYYY-mmm format begins
    [ month, day ] = [ str(date.strftime('%m')), str(date.strftime('%d')) ] # stores the 2 digit month and 2 digit day for date
    data_begin = site_contents.find(month + '/' + day, date_begin) # find where the month and day begin after date_begin
    if data_begin == -1: # if data_begin is -1 (date not found in report)
        x = x + 1 # increment x to go to the next date
    else: # report date is valid
        success = 1 # set success to 1
        value_end = site_contents.find('&nbsp;', data_begin + 7) # stores the index where the data value ends
        value_begin = site_contents.rfind('>', data_begin, value_end) # stores the index of where the data value begins
        value = float(site_contents[value_begin + 1:value_end].replace(',','')) / 1000  # store the value and divide by 1000 to convert thousand barrels to million barrels
        headings = ['Date', 'Crude Oil Stocks (Million Barrels)']
        data = {'Date': [date.strftime('%Y%m%d')], 'Crude Oil Stocks (Million Barrels)': [value]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        quandl_code = 'WEEKLY_US_CRUDE_OIL_STOCKS\r'# build unique quandl code
        reference_text = '  Historical figures from EIA can be verified' \
        '\n  at http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W\n' 
        print 'code: ' + quandl_code + '\n'
        print 'name: Weekly US Crude Oil Stocks \n'
        print 'description: Weekly stocks of crude oil in million barrels'\
        '\n  for the United States. \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=WCESTUS1&f=W\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'