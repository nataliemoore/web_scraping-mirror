# -*- coding: utf-8 -*-
"""
Created on Fri Jun 20 10:47:34 2014

@author: nataliecmoore

Script Name: USDA_NW_PY029_HIST_SCRAPER

Purpose:

Approach:

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/20/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Literal, Word, nums, printables
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re


num_days = 15 # number of previous business days to find data for
startdate = datetime.datetime.now()-0*15*pto.BDay() # the start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing and pyparsing are used to find the section of 
# the website where the data is listed and a table is created to hold the data 
# for that date.
x = 1 
while x <= num_days:
    date = startdate - x * pto.BDay() # subtracts one business day from the start date each iteration
    month = str(date.month) # stores the month as a string    
    if len(month) == 1:  # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # stores date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/NW_PY029' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # This error occurs when a report is missing
        x = x + 1 # increment x
        continue # exit current iteration and go to the next
    start = site_contents.find('\r\n', site_contents.find('(000)')) 
    while True:
        start_label = site_contents.find('\r\n', start + 1) + 2  
        start = start_label
        end_label1 = site_contents.find('  ', start_label)
        point_index = site_contents.rfind('.', 0, end_label1)
        end_label2 = site_contents.rfind(' ', 0, point_index)
        if point_index < start_label:
            end_label2 = end_label1
        end_label = min( [end_label1, end_label2] )
        ending_index = site_contents.find('\r\n', start_label) # index of new line character after label
        if site_contents.find("EXPORT TRADING") == ending_index + 2:
            break # finish iterating when export trading section begins
        empty_line = Literal(site_contents[start_label:end_label]) # empty line is only label with no values following
        nonempty_line = Literal(site_contents[start_label:end_label]) + Word(printables) + Word(nums+'.') + Word(nums) # non empty line has label with 3 data values following
        line_grammar = nonempty_line | empty_line # line can be either empty or non empty
        parsed = line_grammar.parseString(site_contents[start_label:ending_index]) 
        headings = [ 'Date', 'Weighted Average', 'Volume (000)' ]
        if len(parsed) != 1:
            data = { 'Date': [string_date], 'Weighted Average': [parsed[2]], 'Volume (000)': [parsed[3]] }
        else:
            data = { 'Date': [string_date], 'Weighted Average': [0], 'Volume (000)': [0] }
        name = site_contents[start_label:end_label]    
        if  name[len(name) - 1] == '/': # remove the 1/, 2/, 3/ 4/ from end of names if needed
            name = name[:(len(name)-3)]
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        replace = re.compile('[ /-]') # list of characters to be replaced in the pork cut description
        remove = re.compile('[,%#&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
        name1 = replace.sub('_', name) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        quandl_code = 'USDA_NW_PY029_' + name2 + '\r'
        print 'code: ' + quandl_code
        print 'name: Daily National Turkey Parts- ' + name.title() + '\r'
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' 
        print 'description:  Weighted average price and volume of turkey parts' \
        '\n  from the USDA NW_PY029 report published by the USDA Agricultural Marketing Service ' \
        '\n  (AMS). This dataset covers ' + name.lower() + '.\n'\
        + reference_text 
        print 'reference_url: http://www.ams.usda.gov/mnreports/nw_py029.txt'
        print 'frequency: daily'
        print 'private: false'
        print '---'
        data_df.to_csv(sys.stdout)
        print ''
        print ''
    x = x + 1