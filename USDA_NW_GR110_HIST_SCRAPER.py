# -*- coding: utf-8 -*-
"""
Created on Thu Jun 19 16:04:40 2014

@author: nataliecmoore

Script Name: USDA_NW_GR110_HIST_SCRAPER

Purpose:
Retrieve Iowa daily grain prices from the NW_GR110 report via the USDA AMS
web service for the past five business days for upload to Quandl.com. 
The script pulls data for minimum bid, maximum bid, and average bid for 
#2 Yellow Corn and #1 Yellow Soybeans at each Iowa region.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the data for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/19/2014      Natalie Moore   Initial development/release

"""
from pyparsing import Suppress, Literal, Word, nums
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re

num_days = 5 # number of previous business days to find data for
startdate = datetime.datetime.now() # the start date is today's date
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing and pyparsing are used to find the section of the 
# website where the data is listed and a table is created to hold the data for that date.
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtracts one business day from the start date each iteration
    month = str(date.month) # stores the month as a string
    if len(month) == 1:  # if month has a length 1 (month is Jan..Sept)
        month = '0' + month  # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # stores the date in string form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/NW_GR110' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # this error occurs when a report is missing
        x = x + 1 # increment x
        continue # continue to next iteration of the loop
    # list of each location in the report
    labels = [ 'Northwest', 'North Central', 'Northeast', 'Southwest', 'South Central', 'Southeast' ]
    # list of each crop in the report
    crop_labels = [ '#2 Yellow Corn', '#1 Yellow Soybeans' ]
    ending_index = 0 # initializes to 0 so that it can be later used to divide site_contents into sections
    # Loops through each location in labels and finds the data for that location.
    # Pyparsing is used to store each data element in a list so that it can be
    # used to create a table.
    z = 0
    while z < len(labels):
        starting_index = site_contents.find(labels[z], ending_index)
        line = Suppress(Literal(labels[z])) + (Word(nums+'.'+'*') + (Suppress(Literal('\x96')) | Suppress(Literal('-'))) + Word(nums+'.'+'*') + Word(nums+'.'+'*'))*2     
        ending_index = site_contents.find('\r\n', starting_index)
        ending_index1 = site_contents.find('\t', starting_index)
        if ending_index1 != -1:
            ending_index = min([ending_index, ending_index1])
        line = line.parseString(site_contents[starting_index:ending_index])
        # Loops through each crop in crop_labels and creates a table using the location
        # in labels and the crop in crop_labels.     
        y = 0
        while y < len(crop_labels):
            headings = [ 'Date', 'Minimum Bid', 'Maximum Bid', 'Average Bid' ]
            data = { 'Date': [string_date], 'Minimum Bid': [line[0].replace('*','')], 'Maximum Bid': [line[1].replace('*','')], 'Average Bid': [line[2].replace('*','')] }
            del line[0:3]
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
            remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@ ]') # list of characters to be removed from the pork cut description
            name1 = replace.sub('_', crop_labels[y].upper()) # replace certain characters with '_'
            name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
            name2 = name2.translate(None, '-') # ensure '-' character is removed
            quandl_code = 'USDA_NW_GR110_' + labels[z].upper().replace(' ', '_') + '_' + name2 + "\n"
            reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
            'at http://mpr.datamart.ams.usda.gov.\n' 
            print 'code: ' + quandl_code+'\n'
            print 'name: '+ labels[z] + ' Iowa Daily Grain Prices- ' + crop_labels[y].replace('#', '') + '\n'
            print 'description: Daily minimum, maximum, and average bids for ' + labels[z] + ' Iowa ' + crop_labels[y].replace('#', '') + ' from the USDA NW_GR110 report published by the USDA Agricultural Marketing Service ' \
            '(AMS). Prices represent $/bu.' + reference_text
            print 'reference_url: http://www.ams.usda.gov/mnreports/nw_gr110.txt\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
            y = y + 1
        z = z + 1
    x = x + 1
