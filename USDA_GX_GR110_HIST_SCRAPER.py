# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 08:24:46 2014

@author: nataliecmoore

Script Name: USDA_GX_GR110_HIST_SCRAPER

Purpose:
Retrieve USDA data from the GX_GR110 report for the past 5 days via the USDA AMS
web service for upload to Quandl.com. The script pulls data for
the minimum and maximum bids for the past 15 days and past 15-30 days for
soybeans, corn, and soft red winter wheat.

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the minimum and maximum bids for soybeans, corn, and soft red winter wheat 
for each date.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/18/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re

num_days = 5 #set by user of program
startdate = datetime.datetime.now() # start date is today's date
# This function takes in an index of a hyphen and returns the minimum and maximum bids
# Precondition: index is a valid index for a hyphen that separates two numerical values
def min_and_max(index):
    space_before = site_contents.rfind(' ', 0, index)
    space_after = site_contents.find(' ', index)
    minimum_bid = site_contents[space_before:index].strip()
    maximum_bid = site_contents[index+1:space_after].strip()
    return [minimum_bid, maximum_bid]
# Iterates through each date and finds the url for the AMS archived report for
# that day. Then string parsing is used to find the minimum and maximum bids for
# each crop and a table is created to hold the data for that date.   
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day from the start date each iteration
    month = str(date.month) # store the month in string form
    if len(month) == 1: # if the month is Jan..Sept
        month = '0' + month # prepend a 0 to the date
    string_date = date.strftime('%Y%m%d') # store the date in YYYYmmdd format
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       x = x + 1
       continue
   
    """ 
        
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/GX_GR110' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # this error occurs when a report is missing
        x = x + 1 # increment x to skip to the next date
        continue # exit iteration and go to the next
    # Stores the names of the crops for the past 15 days in labels_15 and the names
    # of the crops formatted to be used in the name of the quandl file in labels_15_names
    labels_15 = ['Soybeans', 'Corn', 'Corn']
    labels_15_names = ['Soybeans', 'Corn (Terminal Elevator)', 'Corn (Processor)']
    # Stores the names of the crops for the past 15-30 days in labels_30 and the names
    # of the crops formatted to be used in the name of the quandl file in labels_30_names   
    labels_30 = ['SRW Wheat', 'Soybeans', 'Corn', 'Corn']    
    labels_30_names = ['SRW Wheat', 'Soybeans', 'Corn (Terminal Elevator)', 'Corn (Processor)']
    ending_index = 0 # used in the following loop for indexing, initialized to 0
    # Loops through each crop in labels_15. Finds the minimum and maximum bids
    # and formats for upload to quandl.
    here=[]    
    y = 0
    while y < len(labels_15):
        ending_index = site_contents.find('Spot', ending_index+1) # bids occur before the word "Spot"
        starting_index = site_contents.rfind(labels_15[y], 0, ending_index) # index of the crop name
        hyphen = site_contents.find('-', starting_index) # index of the hyphen that separates the bids
        final_index = site_contents.find(' Spot ', starting_index)
        if hyphen==-1 or hyphen > final_index:
            space_before=site_contents.find('  ', starting_index)
            site_contents_1=site_contents[space_before:].lstrip()
            space_after=site_contents_1.find(' ')+(len(site_contents)-len(site_contents_1))
            value=site_contents[space_before:space_after].strip()
            bids=[value, value]
        else:
            bids = min_and_max(hyphen) # calls min_and_max and stores the minimum and maximum bids in list "bids"
            bids = [float(b.replace('*','')) for b in bids] # changes bid values to floats
        headings = [ 'Date', 'Minimum Bid', 'Maximum Bid']
        data = {'Date': [string_date], 'Minimum Bid': [bids[0]], 'Maximum Bid': [bids[1]]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
        remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
        name1 = replace.sub('_', labels_15_names[y].upper()) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        quandl_code = 'USDA_GX_GR110_' + name2 + '_SPOT\r'
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' 
        print 'code: ' + quandl_code + '\n'
        print 'name: Chicago Terminal Grain Report- Minimum and Maximum Bids (past 15 days)- ' + labels_15_names[y] + '\n'
        print 'description: Minimum and maximum bids up to the past 15 days for '+ labels_15_names[y] + \
        ' from the USDA GX_GR110 report published by the USDA Agricultural Marketing Service ' \
        '(AMS). Prices represent $/bu. \n'\
        + reference_text+'\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/gx_gr110.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1   
    ending_index = 0 # initializes ending_index to 0
    # Loops through each crop in labels_30. Finds the minimum and maximum bids
    # and formats for upload to quandl.  
    y = 0
    while y < len(labels_30):
        ending_index = site_contents.find('30 Days', ending_index+1) # bids occur before the string "30 Days"     
        starting_index = site_contents.rfind(labels_30[y],0, ending_index) # index of the crop name      
        hyphen = site_contents.find('-', starting_index) # index of the hyphen that seperates the bid values
        final_index = site_contents.find('30 Days', starting_index)
        if hyphen==-1 or hyphen >= final_index-2:
            space_before=site_contents.find('  ', starting_index)
            site_contents_1=site_contents[space_before:].lstrip()
            space_after=site_contents_1.find(' ')+(len(site_contents)-len(site_contents_1))
            value=site_contents[space_before:space_after].strip()
            bids=[value, value]
        else:
            bids = min_and_max(hyphen) # calls min_and_max and stores the minimum and maximum bids in list "bids"
            bids = [float(b.replace('*','')) for b in bids] # changes bid values to floats
        headings = [ 'Date', 'Minimum Bid', 'Maximum Bid']
        data = {'Date': [string_date], 'Minimum Bid': [bids[0]], 'Maximum Bid': [bids[1]]}
        data_df = pd.DataFrame(data, columns = headings)
        data_df.index = data_df['Date']
        data_df = data_df.drop('Date', 1)
        replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
        remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
        name1 = replace.sub('_', labels_30_names[y].upper()) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        quandl_code = 'USDA_GX_GR110_' + name2 +'_30_DAY\r'
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' 
        print 'code: ' + quandl_code+'\n'
        print 'name: Chicago Terminal Grain Report- Minimum and Maximum Bids (Past 15-30 days)- ' + labels_30_names[y] + '\n'
        print 'description: Minimum and maximum bids for the past 15-30 days for ' + labels_30_names[y] + \
        ' from the USDA GX_GR110 report published by the USDA Agricultural Marketing Service ' \
        '(AMS). Prices represent $/bu. \n'\
        + reference_text + '\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/gx_gr110.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        data_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1    
    x = x + 1
