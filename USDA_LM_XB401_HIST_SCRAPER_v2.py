# -*- coding: utf-8 -*-
"""
Created on Thu Jun 19 14:35:00 2014

@author: nataliecmoore

Script Name: USDA_LM_XB401_HIST_SCRAPER_v2

Purpose:
Retrieve daily USDA beef data from the LM_XB401 report for the past 5 business 
days via the USDA AMS web service for upload to Quandl.com. The script pulls 
data for weight, min/max price, and weighted average for central beef 
(fresh 90%, frozen 90%, and fresh 85%).

Approach:
Used the USDA AMS (agricultural marketing service) to access archived reports
for the past week. Iterated through each date and then used string indexing to 
obtain the data for each of the dates.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/19/2014      Natalie Moore   Initial development/release

"""

from pyparsing import Word, alphas, printables, nums
import pandas as pd
import pandas.tseries.offsets as pto
import datetime
import urllib2
import sys
import re

num_days = 5 # number of previous business days to find data for
startdate = datetime.datetime.now() # start date is today's date
x = 1
while x <= num_days:
    date = startdate - x * pto.BDay() # subtract one business day from the start date each iteration
    month = str(date.month)    # store month as a string
    if len(month) == 1:  # if month has a length 1 (month is Jan..Sept)
        month = '0' + month # prepend a zero to the month
    string_date = date.strftime('%Y%m%d') # store date in the form YYYYmmdd
    """
    Some dates in the AMS archive don't work correctly (because the incorrect report
    was uploaded, etc.) If that occurs, uncomment the following code and replace 
    '20140428' with the date that isn't working (type >>>string_date into the console)
    and then replace '20140425' with the date of the previous business day.
    
    if string_date == '20140428':
       string_date = '20140425'
   
    """ 
    # This try/except block accounts for reports that are missing from the AMS
    # archive because the USDA was closed during that day and didn't publish 
    # reports (Such as during a government shutdown). 
    try:
        # Find the url of the archived report for 'date' and then store the 
        # contents in site_contents. 
        # The url follows the format: http://search.ams.usda.gov/mndms/YEAR/MONTH/REPORTNAMEYYYYmmdd.TXT
        # where YEAR is the 4 digit year, month is the 2 digit month and report name is the name of the report
        # in AA_AA### format.
        target_url = 'http://search.ams.usda.gov/mndms/' + str(date.year) + '/' + month + '/LM_XB401' + string_date + '.TXT'
        site_contents = urllib2.urlopen(target_url).read()
    except urllib2.HTTPError: # tnis error occurs when a report is missing
        x = x + 1 # increment x to skip that date and go to the next
        continue # go to next iteration of the loop
    names = []                                                   # holds name of each beef grade
    weight = []                                                  # holds weight of each beef grade
    min_price = []                                               # holds minimum price
    max_price = []                                               # holds maximum price
    weighted_average = []                                        # holds weighted_average
    lines = []                                                   # holds each relevant line of data
    starting_index = site_contents.find('Fresh  90%')            # starting index of relevant website section
    ending_index = site_contents.find('Frozen 85%')              # ending index of website section
    break_point = site_contents.find('\r\n', starting_index)     # ending index of first line
    break_point2 = site_contents.find('\r\n', break_point+1)     # ending index of second line
    lines.append(site_contents[starting_index:break_point])      # store website data from starting point to end of first line
    lines.append(site_contents[break_point+4:break_point2])      # store website data from end of first line (after 4 char new line string) to end of second line
    lines.append(site_contents[break_point2+4:ending_index-4])   # store website data from end of second line (after new line) to ending index (before new line)
    # Grammar for each line of the website
    nonempty_line = Word(alphas) + Word(printables) + Word(nums) + Word(printables) + Word(printables) + Word(printables) + Word(printables)
    empty_line = Word(alphas) + Word(printables)
    line = nonempty_line | empty_line
    parsed = []                                                  # holds parsed lines
    # parses each line and adds to parsed list
    y = 0
    while y < len(lines):
        parsed.append(line.parseString(lines[y]))
        y = y + 1
    # stores each piece of data into its respective list    
    y = 0
    while y < len(parsed):
        if len(parsed[y]) != 2:
            names.append(" ".join(parsed[y][0:2]))                 # add first two strings in parsed list to names list
            weight.append(float(parsed[y][3].replace(',', '').replace('*','')))    # store weight in weight list, remove commas and convert to float
            min_price.append(float(parsed[y][4].replace('$', '').replace('*',''))) # store the minimum price in min_price, remove $ and convert to float
            max_price.append(float(parsed[y][5].replace('$', '').replace('*',''))) # store the maximum price in max_price, remove $ and convert to float
            weighted_average.append(float(parsed[y][6].replace('$', '').replace('*',''))) # store weighted average into weighted_average, remove $ and convert to float
        else:
            names.append(' '.join(parsed[y][0:]))
            weight.append(0)
            min_price.append(0)
            max_price.append(0)
            weighted_average.append(0)
        y = y + 1
    # This loops through each piece of data in lines and makes a table in csv format and formats for
    # upload to Quandl.        
    y = 0
    while y < len(lines):
        headings = [ 'Date', 'Weight', '$ Min', '$ Max', 'Weighted Average' ]
        data = { 'Date': [string_date], 'Weight': [weight[y]], '$ Min': [min_price[y]], '$ Max': [max_price[y]], 'Weighted Average': [weighted_average[y]] }
        cl_df = pd.DataFrame(data, columns = headings)
        cl_df.index = cl_df['Date']
        cl_df.drop(['Date'],inplace=True,axis=1) 
        replace = re.compile('[ /]') # list of characters to be replaced in the pork cut description
        remove = re.compile('[,%#-&()!$+<>?/\'"{}.*@]') # list of characters to be removed from the pork cut description
        name1 = replace.sub('_', names[y]) # replace certain characters with '_'
        name2 = remove.sub('', name1).upper() # remove certain characters and convert to upper case
        name2 = name2.translate(None, '-') # ensure '-' character is removed
        reference_text = '  Historical figures from USDA can be verified using the LMR datamart located ' \
        '\n  at http://mpr.datamart.ams.usda.gov.\n' \
        '  All pricing is on a per CWT (100 lbs) basis.'
        print 'code: USDA_LM_XB401_CHEMICAL_LEAN_' + name2
        print 'name: Central Beef' + ' - ' + names[y] + ' ' + '\n'
        print 'description: "Daily values for central beef from the USDA LM_XB401\n' \
        '  report published by the USDA Agricultural Marketing Service (AMS).\n' \
        + reference_text + '"\n'
        print 'reference_url: http://www.ams.usda.gov/mnreports/lm_xb401.txt\n'
        print 'frequency: daily\n'
        print 'private: false\n'
        print '---\n'
        cl_df.to_csv(sys.stdout)
        print '\n'
        print '\n'
        y = y + 1
    x = x + 1
