# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 08:03:11 2014

@author: nataliecmoore

Script Name: USDA_LM_HG201_HIST_SCRAPER

Purpose:

Approach:

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
06/12/2014      Natalie Moore   Initial development/release

"""

import pandas as pd
import pandas.tseries.offsets as pto
import re
import datetime
import urllib2, httplib
import sys
from lxml import objectify

# Set the date range for the report (4 days to ensure all data is captured)
startdate = datetime.datetime.now() - 31 * pto.BDay()
startdate = startdate.strftime('%m/%d/%Y')
print startdate

target_url = 'http://mpr.datamart.ams.usda.gov/ws/report/v1/hogs/LM_HG201?filter={%22filters%22:[{%22fieldName%22:%22Report%20Date%22,%22operatorType%22:%22GREATER%22,%22values%22:[%2204/30/2014%22]}]}'

fileobj = urllib2.urlopen(target_url).read()

root = objectify.fromstring(fileobj)

labels_prod_sold = ['HEAD COUNT', 'CARCASS BASE PRICE', 'AVERAGE NET PRICE', 'LOWEST NET LOT', 'HIGHEST NET LOT', \
        'AVERAGE LIVE WT', 'AVERAGE CARCASS WT', 'AVERAGE SORT LOSS', 'AVERAGE BACKFAT', 'AVERAGE LOIN DEPTH (LD)', \
        'LOINEYE AREA (LD Converted)', 'AVERAGE LEAN PERCENT']
labels_pack_sold = ['HEAD COUNT', 'CARCASS BASE PRICE', 'AVERAGE NET PRICE','AVERAGE OF LOWEST NET LOTS', \
        'AVERAGE OF HIGHEST NET LOTS', 'AVERAGE LIVE WT', 'AVERAGE CARCASS WT', 'AVERAGE SORT LOSS', \
        'AVERAGE BACKFAT', 'AVERAGE LOIN DEPTH (LD)', 'LOINEYE AREA (LD Converted)', 'AVERAGE LEAN PERCENT']
labels_pack_owned = ['HEAD COUNT', 'AVERAGE LIVE WT', 'AVERAGE CARCASS WT', 'AVERAGE BACKFAT', 'AVERAGE LOIN DEPTH (LD)', 'AVERAGE LEAN PERCENT']
    #store all of the section labels in one list
data=[]

for report_date in root.report.iterchildren(): #processing must be repeated for each day covered by the report
    date = report_date.attrib['for_date_begin']
    for report in report_date.iterchildren():
        if report.attrib['label']=='Barrows/Gilts':
            for item in report.iterchildren():
                data.append([item.attrib])