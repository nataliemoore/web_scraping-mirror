# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 12:35:58 2014

@author: nataliecmoore

Script Name: EIA_STOR_WK_HIST_SCRAPER

Purpose:
Find the amount of natural gas in underground storage for the past 3 weeks
for the regions: eastern, lower 48 states, producting region, and western
region.

Approach:
Looped through the reports for each region and found the 3 most recent storage
reports published through the Energy Information Administration.

Author: Natalie Moore

History:

Date            Author          Purpose
-----           -------         -----------
07/07/2014      Natalie Moore   Initial development/release

"""

import urllib2
import datetime
import pandas as pd
import sys
# Creates a list to store the 4 urls corresponding to the underground storage data
# for each of the four regions
url1 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R88_BCF.W'
url2 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R48_BCF.W'
url3 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R87_BCF.W'
url4 = 'http://www.eia.gov/beta/api/qb.cfm?category=478126&sdid=NG.NW_EPG0_SAO_R89_BCF.W'
url_list = [url1, url2, url3, url4] 
region_list = ['Eastern Region', 'Lower 48 States', 'Producing Region', 'Western Region']
num_days = 25 # the number of previous days to find data for
# Loops through each url in url_list and finds the data for each day going back num_days
u = 0
while u < len(url_list):
    site_contents = urllib2.urlopen(url_list[u]).read() # stores the current url's contents in site_contents
    startdate = datetime.datetime.now() # the startdate is today's date
    # Loops through each day and creates a quandl data table for that day    
    z = 0
    while z < num_days:
        date = startdate - datetime.timedelta(days = z) # subtracts one day from the start date each iteration
        year = str(date.year) # stores the year as a string
        # One month is subtracted from the current month to convert a month in the normal rnage 1..12 to the range
        # 0..11. The month is stored in the report with 0 corresponding to Jan, 1 corresponding to Feb, etc.
        month = str(date.month - 1) 
        day = str(date.day) # stores the day as a string
        if len(day) == 1: # if the day is one digit
            day = '0' + day # prepend a 0 to make the day 2 digits
        # Find the date in the report. It is stored in the format '[Date.UTC(YYYY, mm, dd)'
        date_begin = site_contents.find('[Date.UTC(' + year + ', ' + month + ', ' + day + '),')
        if date_begin == -1: # If the current date is not found in the report
            z = z + 1 # increment z
            continue # continue to next iteration to try the next day
        else:
            date_end = site_contents.find('),', date_begin) # store index where the date ends
            value_end = site_contents.find(']', date_end) # store the index where the storage value ends
            value = site_contents[date_end + 2:value_end] # store the storage value
            headings = ['Date', 'Natural Gas (Billion Cubic Feet)']
            data = {'Date': [date.strftime('%Y-%m-%d')], 'Natural Gas (Billion Cubic Feet)': [value]}
            data_df = pd.DataFrame(data, columns = headings)
            data_df.index = data_df['Date']
            data_df = data_df.drop('Date', 1)
            quandl_code = 'WEEKLY_NAT_GAS_UNDERGROUND_STORAGE_' + region_list[u].replace(' ', '_').upper() + '\r'# build unique quandl code
            reference_text = '  Historical figures from EIA can be verified' \
            '\n  at http://www.eia.gov/beta/api/qb.cfm?category=371\n' 
            print 'code: ' + quandl_code + '\n'
            print 'name: Weekly Natural Gas Working Underground Storage- ' + region_list[u] + '\n'
            print 'description: Weekly amount of natural gas in underground storage'\
            '\n  for the ' + region_list[u] + '. \n'\
            + reference_text + '\n'
            print 'reference_url: http://www.eia.gov/dnav/ng/ng_stor_wkly_s1_w.htm\n'
            print 'frequency: daily\n'
            print 'private: false\n'
            print '---\n'
            data_df.to_csv(sys.stdout)
            print '\n'
            print '\n'
            z = z + 1
    u = u + 1


